# Merx Online

# Contribution

Comme le suggère le côté open source du projet, les contributions sont acceptées et même très appréciées.
Pour savoir comment contribuer, consultez le fichier CONTRIBUTING.md.

# Mise en place du projet

Cette section vous présente comment récupérer le projet et le lancer. 

Pour pouvoir lancer le projet vous aurez besoin :

	visual studio 2017
	Unity (normalement la derniere version)

La marche à suivre : 
- Cloner le projet 
- ouvrir la solution \Server\solutions\vs2017\MerxOnlineCommon\MerxOnlineCommon.sln
- Choisir une configuration DebugUnity ou ReleaseUnity
- La compiler (normalement les binaires nécessaires seront copiés dans le dossier \ClientUnity\MerxOnlineClient\Assets\Plugins
- ouvrir la scene \ClientUnity\MerxOnlineClient\Assets\LoginScene.unity
- faire play....