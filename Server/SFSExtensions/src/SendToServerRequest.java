import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.entities.*;;


/**
 * Ce request permet a un client d'envoyer un message au serveur dans la zone
 * @author FX10062
 *
 */
public class SendToServerRequest extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User sender, ISFSObject args) {
		
		int userID = -1;
		try {
			userID = DataManager.GetPlayerID(sender.getName(),getParentExtension().getParentZone().getDBManager());
		} catch (Exception e) {
			System.out.println(e);
		}
		String cmdName = args.getUtfString("messageName");

		
	    if(userID == -1) {
	    	return;
	    }
	    
		//trouver le serveur
		Room room = this.getParentExtension().getParentRoom();
		System.out.println("SendToServer " + cmdName + " from " + sender.getName() + " in room " + room.getName());

		
		User server = room.getUserByName("Server");
		if(server == null)
			return;
		
		args.putInt("playerID", userID);
		System.out.println("sending to " + server.getName());
		send("sendToServer",args,server);
	}

}
