import com.smartfoxserver.v2.extensions.SFSExtension;

/**
 * Cette classe represente l'extension qu'on assigne aux room permettant aux clients de communiquer
 * avec les serveur
 * 
 *
 */
public class OpenMerxUserRoomExtension extends SFSExtension {

	@Override
	public void init() {

		System.out.println("***************************");
		System.out.println(" loaded user room extension  ");
		System.out.println("***************************");
		
		addRequestHandler("sendToServer", SendToServerRequest.class);
		addRequestHandler("sendToClients", SendToClientsRequest.class);
	}

}
