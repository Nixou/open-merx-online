
import java.sql.SQLException;
import com.smartfoxserver.bitswarm.sessions.ISession;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.exceptions.SFSErrorCode;
import com.smartfoxserver.v2.exceptions.SFSErrorData;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.exceptions.SFSLoginException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
//import com.smartfoxserver.v2.extensions.ExtensionLogLevel;

/**
 * 
 * @author NDrew
 * 
 * class qui gere les message de login sur le serveur
 * Il va chercher le user dans la base de donn� configur� dans smartfox et l'ID si le mot de passe est bon
 * 
 *
 */
public class LoginEventHandler extends BaseServerEventHandler {

   @Override
   public void handleServerEvent(ISFSEvent event) throws SFSException
   {
       String userName = (String) event.getParameter(SFSEventParam.LOGIN_NAME); 
       String password = (String) event.getParameter(SFSEventParam.LOGIN_PASSWORD);
       ISession session = (ISession) event.getParameter(SFSEventParam.SESSION);	  
       
       ISFSObject outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
       
       // Get password from DB
       IDBManager dbManager = getParentExtension().getParentZone().getDBManager();
       trace("login tentative for " + userName);
       
       try	{
	        //Build and execute a prepared sql query
	        String sql = "SELECT id, password FROM users WHERE username=?";
	        ISFSArray res = dbManager.executeQuery(sql, new Object[] {userName});
	        
			// Verify that one record was found
	        if(res.size() != 1)
			{
				// This is the part that goes to the client
				SFSErrorData errData = new SFSErrorData(SFSErrorCode.LOGIN_BAD_USERNAME);
				errData.addParameter(userName);
				
				// This is logged on the server side
				throw new SFSLoginException("Bad user name: " + userName, errData);
			}
			
	        ISFSObject val = res.getSFSObject(0);
			String dbPword = val.getUtfString("password");
			int userId = val.getInt("id");
			
			// Verify the secure password
			if (!getApi().checkSecurePassword(session, dbPword, password))
			{
				SFSErrorData data = new SFSErrorData(SFSErrorCode.LOGIN_BAD_PASSWORD);
				data.addParameter(userName);
				
				throw new SFSLoginException("Login failed for user: "  + userName, data);
			}
			
			// Store the client dbId in the session
			//session.setProperty(DBLogin.DATABASE_ID, dbId);
			trace("logged " + userName );
			outData.putInt("ID", userId); 
       }
       catch (SQLException e)
       {
       	SFSErrorData errData = new SFSErrorData(SFSErrorCode.GENERIC_ERROR);
       	errData.addParameter("SQL Error: " + e.getMessage());
       	
       	throw new SFSLoginException("A SQL Error occurred: " + e.getMessage(), errData);
       }

   }
   
}
