import java.sql.SQLException;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class DataManager {

	public static int GetPlayerID(String username, IDBManager dbManager ) throws Exception{
		
		String sql = "SELECT id FROM users WHERE username='"+username+"';";
	     
	   
		try {
	
			ISFSArray res = dbManager.executeQuery(sql, new Object[] {});
	       
			if(res.size() != 1) {
				throw new Exception("User does not exist");
			}
			ISFSObject val = res.getSFSObject(0);
	       
	       return val.getInt("id");     
	   } catch (SQLException e) {
	       System.out.println("SQL Failed: " + e.toString());
	   }
   
		return -1;
	}
}
