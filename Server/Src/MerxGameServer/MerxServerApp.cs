using MerxData;
using MerxMessage;
using MerxServer.Interfaces;
using MerxStructures;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;

namespace MerxServer {
    /// <summary>
    /// classe representant une instance de serveur du jeu. La gestion du jeu est effectu�e dans un thread separ� pour ne pas 
    /// bloquer l'appelant.
    /// </summary>
    public class MerxServerApp : IMerxServerApp {

        private Thread _thread = null;
        private long _currentFrame = 0;
        private bool _stop = false;

        private long _msPerFrame = 10;
        private int _threadLoopSleep = 100;


        private UniverseManager _universeManager = null;

        private delegate void MessageProcessAction(Message m);
        private Dictionary<System.Type, MessageProcessAction> _messageProcessFunctions = new Dictionary<System.Type, MessageProcessAction>();
        private readonly ILogger _logger;
        private readonly IEventSender _eventSender;

        public UniverseManager UniverseManager { get { return _universeManager; } private set { _universeManager = value; } }

        public MerxServerApp(ILogger<MerxServerApp> logger, IEventSender eventSender) {
            _logger = logger;
            _eventSender = eventSender;
        }

        public void InitMerxServerTuningLoop(long msPerFrame, int threadLoopSleep) {
            _threadLoopSleep = threadLoopSleep;
            _msPerFrame = msPerFrame;
        }

        public void InitUniverseManager(byte[] data) {
            _universeManager = new UniverseManager(data);
        }
        public void InitUniverseManager() {
            _universeManager = new UniverseManager();
        }

        /// <summary> lancer la simulation du jeu </summary>
        public void Start() {

            _logger.LogInformation("Server Starting");
            _messageProcessFunctions.Clear();

            #region MessageTypes
            _messageProcessFunctions.Add(typeof(RequestAssembleShip), ProcessAssembleShip);
            _messageProcessFunctions.Add(typeof(RequestUniverseTime), ProcessRequestUniverseTime);
            _messageProcessFunctions.Add(typeof(RequestHangarList), ProcessRequestHangarList);
            _messageProcessFunctions.Add(typeof(RequestCities), ProcessRequestCities);
            _messageProcessFunctions.Add(typeof(RequestBuyHangar), ProcessBuyHangar);
            _messageProcessFunctions.Add(typeof(RequestMarketDatas), ProcessMarketData);
            _messageProcessFunctions.Add(typeof(RequestFillOrders), ProcessFillOrders);
            _messageProcessFunctions.Add(typeof(RequestCorporationUpdate), ProcessCorporationUpdate);
            _messageProcessFunctions.Add(typeof(RequestRepackageVehicle), ProcessRepackageVehicle);
            _messageProcessFunctions.Add(typeof(RequestResourceMove), ProcessResourceMove);
            _messageProcessFunctions.Add(typeof(RequestExpeditionStart), ProcessExpeditionStart);
            _messageProcessFunctions.Add(typeof(RequestMailBoxUpdate), ProcessMailBoxUpdate);
            _messageProcessFunctions.Add(typeof(RequestMailBoxDeleteMessage), ProcessMailBoxDeleteMessage);
            _messageProcessFunctions.Add(typeof(RequestOrganisationCreate), ProcessOrganisationCreate);
            _messageProcessFunctions.Add(typeof(RequestMailBoxReadMail), ProcessMailBoxReadMail);
            #endregion

            _stop = false;
            _thread = new Thread(new ThreadStart(ThreadLoop));
            _thread.Start();
        }

        //demande au serveur de se terminer.
        public void Stop() {
            _stop = true;
        }

        /// <summary>
        /// recuperer la serializatino de l'univers pour une sauvegarde
        /// </summary>
        /// <returns></returns>
        public byte[] GetSaveData() {
            try {
                return _universeManager.SaveUniverse();
            } catch (System.Exception) {
                _logger.LogError("Save Error");
            }
            return null;
            
        }

        /// <summary> la boucle utilis� par le thread</summary>
        private void ThreadLoop() {

            System.DateTime start = System.DateTime.Now;
            _currentFrame = 0;
            while (!_stop) {
                //faire avancer le temps
                System.TimeSpan timeSpan = System.DateTime.Now - start;
                long targetFrame = (timeSpan.Milliseconds + timeSpan.Seconds * 1000 + timeSpan.Minutes * 60000 + timeSpan.Hours * 3600000) / _msPerFrame;
                while (targetFrame > _currentFrame) {
                    _universeManager.UpdateOneFrame();
                    _currentFrame++;
                }

                //envoyer les messages que l'univers a genere
                while (UniverseManager.Universe.messageToSend.Count > 0) {
                    _logger.LogInformation("Queue messageToSend : " + UniverseManager.Universe.messageToSend.Count);
                    _eventSender.SendMessage(UniverseManager.Universe.messageToSend.Dequeue());
                }
                Thread.Sleep(_threadLoopSleep);
            }

            //todo : save
            _thread = null;
        }

        public void ProcessMessage(Message m) {
            _logger.LogInformation("New Processing message " + m.ToString());
            if (_messageProcessFunctions.ContainsKey(m.GetType())) {
                _messageProcessFunctions[m.GetType()](m);
            }
        }

        private void ProcessCorporationUpdate(Message m) {
            RequestCorporationUpdate r = m as RequestCorporationUpdate;
            r.IsAnswer = true;

            Corporation corp = _universeManager.Universe.GetCorporation(r.corpID);
            if (null != corp) {
                r.corpInfo = new CorporationInfos();
                r.corpInfo.CorpID = r.corpID;
                r.corpInfo.Name = corp.Name;
                r.corpInfo.Icu = corp.Icu;
                r.corpInfo.HomeCity = corp.HomeCity.ID;
            }

            // n'envoyer qu'au client qui demande
            m.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessRepackageVehicle(Message m) {
            RequestRepackageVehicle r = m as RequestRepackageVehicle;
            if (null == r)
                return;

            City s = _universeManager.Universe.GetCity(r.cityID);
            Hangar h = s.GetHangar(m.FromPlayerID);

            // todo tester si le vaisseau est la
            h.RepackageVehicle(r.vehicleID);

        }

        private void ProcessResourceMove(Message m) {
            RequestResourceMove r = m as RequestResourceMove;
            r.IsAnswer = true;
            r.success = false;

            try {
                City fromCity = _universeManager.Universe.GetCity(r.cityFrom);
                City toCity = _universeManager.Universe.GetCity(r.cityTo);

                int corpID = r.FromPlayerID;

                Hangar hangarFrom = fromCity.GetHangar(corpID);
                Hangar hangarTo = toCity.GetHangar(corpID);

                if (null != hangarFrom && null != hangarTo && hangarFrom.GetResourceCount(r.resType) >= r.qte) {
                    hangarFrom.AddResource(r.resType, -r.qte);
                    hangarTo.AddResource(r.resType, r.qte);
                    r.success = true;
                }
            } catch (System.Exception e) {
                System.Console.WriteLine(e.ToString());
            }

            _eventSender.SendMessage(r);
        }

        private void ProcessFillOrders(Message m) {
            RequestFillOrders r = m as RequestFillOrders;
            r.IsAnswer = true;

            MarketOrder o = r.order;
            if (o.buying) {
                // le joueur vend des resources
                City s = _universeManager.Universe.GetCity(o.cityID);

                Hangar playerHangar = s.GetHangar(r.FromPlayerID);
                Hangar buyerHangar = s.GetHangar(r.order.corpID);

                if (null != playerHangar && null != buyerHangar) {
                    Corporation corp = _universeManager.Universe.GetCorporation(r.FromPlayerID);

                    //todo valider
                    long price = r.qte * o.price;
                    if (r.qte > o.qte)
                        r.qte = o.qte;

                    int qtePresent = playerHangar.GetResourceCount(o.resID);
                    if (r.qte > qtePresent)
                        r.qte = qtePresent;

                    //mettre a jour
                    playerHangar.AddResource(r.order.resID, -r.qte);
                    buyerHangar.AddResource(r.order.resID, r.qte);

                    corp.Icu += price;

                    o.qte -= r.qte;
                    if (o.qte <= 0) {
                        _universeManager.Universe.Market.RemoveOrder(o);
                    } else {
                        _universeManager.Universe.Market.UpdateOrder(o);
                    }
                }
            } else {
                //le joueur achete quelque chose
                City s = _universeManager.Universe.GetCity(o.cityID);
                int playerCorp = r.FromPlayerID;
                Hangar h = s.GetHangar(playerCorp);

                Hangar sellerHangar = s.GetHangar(r.order.corpID);
                sellerHangar.UnlockResources(r.order.resID, r.qte);
                sellerHangar.RemoveResources(r.order.resID, r.qte);

                Corporation corp = _universeManager.Universe.GetCorporation(playerCorp);

                //test des prerequis
                if (r.qte > o.qte)
                    r.qte = o.qte;

                int price = r.qte * o.price;
                if (h != null && price <= corp.Icu) {

                    //on effectue l'echange
                    h.AddResource(o.resID, r.qte);

                    o.qte -= r.qte;
                    if (o.qte <= 0)
                        _universeManager.Universe.Market.RemoveOrder(o);
                    else {
                        //mettre a jour le market
                        _universeManager.Universe.Market.UpdateOrder(o);
                    }

                    corp.Icu -= price;
                }
            }

            _eventSender.SendMessage(r);
        }

        private void ProcessAssembleShip(Message m) {
            RequestAssembleShip r = m as RequestAssembleShip;
            if (null == r)
                return;

            City s = _universeManager.Universe.GetCity(r.cityID);
            Hangar h = s.GetHangar(m.FromPlayerID);

            h.AssembleVehicle(r.shipType);

            _eventSender.SendMessage(r);
        }

        private void ProcessRequestUniverseTime(Message m) {
            RequestUniverseTime r = m as RequestUniverseTime;
            r.IsAnswer = true;
            r.Frame = _universeManager.Universe.Frame;

            m.SetTargetToPlayerID(m.FromPlayerID);
            _eventSender.SendMessage(r);
        }

        private void ProcessRequestCities(Message m) {
            RequestCities r = m as RequestCities;
            r.IsAnswer = true;
            _logger.LogInformation("processing request cities");

            Universe universe = UniverseManager.Universe;
            foreach (City s in universe.GetCities().Values) {
                CityInfo info = new CityInfo();
                info.ID = s.ID;
                info.Name = s.Name;
                info.Hangars = new List<int>(s.GetHangars().Keys);
                r.cities.Add(info);
                foreach (StarGate g in s.Gates.Values) {
                    if (g.TargetCity.ID > s.ID) {
                        r.gatesFrom.Add(s.ID);
                        r.gatesTo.Add(g.TargetCity.ID);
                    }
                }
            }
            _logger.LogInformation("sending cities");

            // on l'envoie qu'au joueur qui demande
            r.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessMarketData(Message m) {
            RequestMarketDatas r = m as RequestMarketDatas;
            if (r.IsAnswer)
                return;

            r.IsAnswer = true;
            r.Orders = _universeManager.Universe.Market.GetOrders(r.resourceID);

            r.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessRequestHangarList(Message m) {
            RequestHangarList r = m as RequestHangarList;
            r.IsAnswer = true;


            Universe universe = UniverseManager.Universe;
            Dictionary<int, City> cities = universe.GetCities();

            foreach (int key in cities.Keys) {
                City city = cities[key];
                Dictionary<int, Hangar> hangars = city.GetHangars();
                foreach (int hangarKey in hangars.Keys) {
                    Hangar h = hangars[hangarKey];
                    if (h.CorpID == m.FromPlayerID) {
                        HangarInfos infos = h.GetInfos();
                        r.hangars.Add(infos);
                    }
                }
            }

            // envoyer seulement a celui qui la demande
            m.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessExpeditionStart(Message m) {
            RequestExpeditionStart r = m as RequestExpeditionStart;
            if (null == r)
                return;

            City s = UniverseManager.Universe.GetCity(r.fromCityID);
            if (null == s)
                return;

            Hangar h = s.GetHangar(r.fromHangarID);
            if (null == h)
                return;

            List<Vehicle> vehicles = new List<Vehicle>();
            foreach (int i in r.vehicles) {
                Vehicle v = h.GetVehicle(i);
                if (null != v) {
                    vehicles.Add(v);
                }
            }

            Expedition expe = new Exploration(_universeManager.Universe);
            expe.Start(s, h, vehicles);

            _eventSender.SendMessage(r);
        }

        private void ProcessBuyHangar(Message m) {
            RequestBuyHangar r = m as RequestBuyHangar;
            r.IsAnswer = true;

            //trouver la corporation du joueur
            int corpID = r.FromPlayerID;

            //trouver la city
            City s = _universeManager.Universe.GetCity(r.CityID);
            if (null == s)
                return;

            Hangar h = s.AddHangar(corpID);
            if (null == h)
                return;

            r.HangarID = h.ID;
            r.Hangar = new HangarInfos(h.ID, h.CorpID, h.City.ID);

            m.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessMailBoxUpdate(Message m) {
            RequestMailBoxUpdate r = m as RequestMailBoxUpdate;
            if (null == r)
                return;

            Corporation c = UniverseManager.Universe.GetCorporation(m.FromPlayerID);
            if (null == c)
                return;


            MailBox mailbox = c.GetMailBox();

            MailBoxInfos infos = new MailBoxInfos();
            foreach (MailInfos i in mailbox._mails) {
                infos.mails.Add(i);
            }

            r.infos = infos;

            // ne doit etre envoye qu'a la personne qui demande
            m.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessOrganisationCreate(Message m) {
            RequestOrganisationCreate r = m as RequestOrganisationCreate;
            if (null == r)
                return;

            int playerID = m.FromPlayerID;

            //tester si le joueur a une corporation
            Corporation c = _universeManager.Universe.GetCorporation(playerID);

            if (null == c) {
                //pas d'organisation on doit la creer
                c = _universeManager.Universe.CreateCorporation(playerID,r.Name,r.CityID);
            }

            if (null == c) {
                r.CityID = -1;
            } else {
                r.CityID = c.ID;
            }

            r.SetTargetToPlayerID(playerID);

            _eventSender.SendMessage(r);
        }            

        private void ProcessMailBoxDeleteMessage(Message m) {
            RequestMailBoxDeleteMessage r = m as RequestMailBoxDeleteMessage;
            if (null == r)
                return;

            Corporation corp = UniverseManager.Universe.GetCorporation(m.FromPlayerID);
            if (null == corp)
                return;

            MailBox mailBox = corp.GetMailBox();
            if (null == mailBox)
                return;

            mailBox.DeleteMail(r.Mail);

            r.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }

        private void ProcessMailBoxReadMail(Message m) {
            RequestMailBoxReadMail r = m as RequestMailBoxReadMail;
            if (null == r)
                return;

            Corporation corp = UniverseManager.Universe.GetCorporation(m.FromPlayerID);
            if (null == corp)
                return;

            MailBox mailBox = corp.GetMailBox();
            if (null == mailBox)
                return;

            mailBox.ReadMail(r.Id);

            r.SetTargetToPlayerID(m.FromPlayerID);

            _eventSender.SendMessage(r);
        }
    }
}