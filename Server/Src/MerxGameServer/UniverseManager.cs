using MerxData;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MerxServer {

    /// <summary>
    /// Classe qui gere l'instanciation, la sauvegarde et la mise a jour de l'univers
    /// </summary>
    public class UniverseManager {

        /// <summary>
        /// l'univers que gere ce manager
        /// </summary>
        Universe _universe = null;

        /// <summary>
        /// constructeur d'un manager qui instancie un nouvel univers
        /// </summary>
        public UniverseManager() {
            _universe = new Universe(1);
        }

        /// <summary>
        /// construction d'un manager a partir de donn�e binaire serialise
        /// </summary>
        /// <param name="data"></param>
        public UniverseManager(byte[] data) {
            MemoryStream stream = new MemoryStream(data);

            int len = data.Length;
            if (len > 0) {
                BinaryFormatter s = new BinaryFormatter();
                _universe = s.Deserialize(stream) as Universe;
            }
        }

        /// <summary>
        /// l'univers gere par ce manager
        /// </summary>
        public Universe Universe { get { return _universe; } private set { _universe = value; } }

        /// <summary>
        /// permet de recuperer les donn�es de l'etat actuel d'un univers pour une reinstanciation ult�rieure
        /// </summary>
        /// <returns></returns>
        public byte[] SaveUniverse() {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(stream, _universe);
            stream.Position = 0;
            return stream.ToArray();
        }

        /// <summary>
        /// indique de faire progresser l'univers de 1 frame
        /// </summary>
        public void UpdateOneFrame() {
            if(null != _universe) {
                _universe.PlayOnFrame();
            }
        }
    }
}