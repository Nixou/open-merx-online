using MerxMessage;

namespace MerxServer {

    /// <summary>
    /// interface que doit implementer un objet qui permet d'envoyer des events vers les clients
    /// </summary>
    public interface IEventSender {

        /// <summary>
        /// envoyer un evenement vers les clients
        /// </summary>
        /// <param name="message"> le message contenant l'evenement </param>
        void SendMessage(Message message);

    }
}