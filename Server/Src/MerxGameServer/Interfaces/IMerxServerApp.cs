using MerxMessage;

namespace MerxServer.Interfaces {

   
    public interface IMerxServerApp
    {

        /// <summary>
        /// envoyer un evenement vers les clients
        /// </summary>
        /// <param name="message"> le message contenant l'evenement </param>
        void Start();
        void InitMerxServerTuningLoop(long msPerFrame, int threadLoopSleep);
        void InitUniverseManager(byte[] data);
        void InitUniverseManager();
        void Stop();
        byte[] GetSaveData();
        void ProcessMessage(Message m);

    }
}