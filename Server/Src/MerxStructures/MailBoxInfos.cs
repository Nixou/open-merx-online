using System;
using System.Collections.Generic;

namespace MerxStructures {

    [Serializable]
    public class MailInfos {
        public long SentFrame;
        public int CorpFrom;
        public string Subject;
        public string Content;
        public bool Read;
        public int Id;

        public MailInfos() { }

        public MailInfos(string subject, string content) {
            Subject = subject;
            Content = content;
        }
    }

    [Serializable]
    public class MailBoxInfos {
        public List<MailInfos> mails = new List<MailInfos>();
    }
}