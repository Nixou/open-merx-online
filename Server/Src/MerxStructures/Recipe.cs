using System;
using System.Collections.Generic;

namespace MerxStructures {

    /// <summary>
    /// Une recette donne les information sur une possibilite de production que peut effectuer une factory
    /// Chaque fois qu'on execute une recette on retire du hangar les inputs, on attend le temps et 
    /// a la fin on ajoute au hangar les outputs
    /// </summary>
    [Serializable]
    public class Recipe {

        /// <summary> le temps en frame que dure la recette </summary>
        public int Time;
        /// <summary> liste des inputs necessaire pour lancer la recette </summary>
        public Dictionary<int, int> inputs = new Dictionary<int, int>();
        /// <summary> liste des outputs que genere cette recette quand elle a terminee </summary>
        public Dictionary<int, int> outputs = new Dictionary<int, int>();
    }
}