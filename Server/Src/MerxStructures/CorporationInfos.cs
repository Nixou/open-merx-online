using System;

namespace MerxStructures {

    /// <summary>
    /// structure representant les informations d'une corporation, elle est utilis� lors de
    /// communications entre clients et serveurs
    /// </summary>
    [Serializable]
    public class CorporationInfos {
        public int CorpID;
        public string Name;
        public int HomeCity;
        public long Icu;
    }
}