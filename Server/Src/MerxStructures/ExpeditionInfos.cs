using System;
using System.Collections.Generic;

namespace MerxStructures {

    /// <summary>
    /// informations represenant une city, elle est utilis�e lors des communications entre
    /// clients et serveurs
    /// </summary>
    [Serializable]
    public struct ExpeditionInfos {
        /// <summary> l'identifiant unique de cette expedition </summary>
        public int ID;

        public List<int> Vehicles;
    }
}