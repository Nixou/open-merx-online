﻿namespace MerxUtils{

    [System.Serializable]
	public class MerxRandom {

		private int mSeed = 0;

		public MerxRandom(){
			mSeed = 0;
		}

		public MerxRandom(int seed){
			mSeed = seed % 32749;
		}

		public int GetInt(){
			mSeed = (mSeed * 32719 + 3) % 32749;
			return mSeed;
		}

		//random number from 1 to maxValue included
		public int Range(int maxValue){
			return(GetInt() % maxValue + 1);
		}

		public int Range(int minValue ,int maxValue){
			return (Range(maxValue - minValue + 1) + minValue - 1);
		}
	}
}
