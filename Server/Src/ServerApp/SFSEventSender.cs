﻿using System;
using System.Collections.Generic;
using MerxMessage;
using MerxServer;
using Microsoft.Extensions.Logging;
using Sfs2X;
using Sfs2X.Requests;

//
// L'application responsable de la gestion du serveur, pour le moment elle est unique et ne lancer aucun autre processus. 
//
namespace ServerApp {
    public class SFSEventSender : IEventSender {

        private readonly ILogger _logger;
        SFSInstance _smartfoxInstance = null;


        public SFSEventSender(ILogger<SFSEventSender> logger, SFSInstance smartFoxInstance) {
            _logger = logger;
            _smartfoxInstance = smartFoxInstance;
        }
        
        public void SendMessage(Message message) {
            byte[] bytes = message.Serialize();

            Sfs2X.Entities.Data.SFSObject obj = new Sfs2X.Entities.Data.SFSObject();

            Sfs2X.Util.ByteArray array = new Sfs2X.Util.ByteArray(bytes);

            obj.PutByteArray("message", array);

            switch (message.Target) {
                case Message.TargetType.Target_All:
                    obj.PutInt("TargetType", 1);
                    break;
                case Message.TargetType.Target_Player:
                    obj.PutInt("TargetType", 2);
                    obj.PutInt("TargetID", message.TargetID);
                    break;
            }

            obj.PutUtfString("MsgName", message.GetType().ToString());

            _smartfoxInstance.SmartFox.Send(new ExtensionRequest("sendToClients", obj, _smartfoxInstance.SmartFox.LastJoinedRoom));

            _logger.LogInformation("Sending : " + message.ToString());
        }
    }
}
