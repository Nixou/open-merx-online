﻿using System;

using MerxServer;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Logging;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Logging;
using MerxServer.Interfaces;
using ServerApp.Config;
using Microsoft.Extensions.Options;

//
// L'application responsable de la gestion du serveur, pour le moment elle est unique et ne lancer aucun autre processus. 
//
namespace ServerApp {
    public class SFSConnection {

        private readonly ILogger _logger;
        private readonly IEventSender _sender;
        private readonly ServerSettings _configuration;
        private readonly SFSInstance _SFSInstance;
        private bool _ready = false;
        private DateTime _lastSend = DateTime.Now;

        public delegate void OnNewMessageAction(MerxMessage.Message m);
        public event OnNewMessageAction OnNewMessage = delegate { };

        public SFSConnection(ILogger<SFSConnection> logger, IEventSender eventSender, IOptions<ServerSettings> configuration, SFSInstance instanceSmartFox) {

            _logger = logger;
            _sender = eventSender;
            _configuration = configuration.Value;
            _SFSInstance = instanceSmartFox;
            _lastSend = DateTime.Now;
            SmartFoxInitListeners();
        }

        public void Connect() {
            Sfs2X.Util.ConfigData cfg = new Sfs2X.Util.ConfigData();
            cfg.Host = _configuration.Host;
            cfg.Port = int.Parse(_configuration.Port);
            cfg.Zone = _configuration.Zone;
            cfg.Debug = false;
            _SFSInstance.SmartFox.Connect(cfg);

            string str = "Starting connection to " + cfg.Host + ":" + cfg.Port + " - zone " + cfg.Zone;
            _logger.LogInformation(str);
            Console.WriteLine(str);


        }

        public void Process() {
            if (_SFSInstance.SmartFox != null) {
                _SFSInstance.SmartFox.ProcessEvents();
            }
            if ((DateTime.Now - _lastSend).Seconds > 30) {
                MerxMessage.Message m = new MerxMessage.RequestUniverseTime();
                m.SetTargetToPlayerID(-1);
                _sender.SendMessage(m);
                _lastSend = DateTime.Now;
            }
        }

        public bool Ready() {
            return _ready;
        }

        #region Smartfox init/reset Listener
        private void SmartFoxInitListeners() {
            //Déclaration des messages pouvant être reçus
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.CONNECTION, OnConnection);
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.LOGIN, OnLogin);
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);
            _SFSInstance.SmartFox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtension);

            _SFSInstance.SmartFox.AddLogListener(Sfs2X.Logging.LogLevel.INFO, OnInfoMessage);
            _SFSInstance.SmartFox.AddLogListener(Sfs2X.Logging.LogLevel.WARN, OnWarnMessage);
            _SFSInstance.SmartFox.AddLogListener(Sfs2X.Logging.LogLevel.ERROR, OnErrorMessage);
        }

        private void Reset() {

            // Remove SFS2X listeners
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.CONNECTION, OnConnection);
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.LOGIN, OnLogin);
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);
            _SFSInstance.SmartFox.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtension);

            _SFSInstance.SmartFox.RemoveLogListener(Sfs2X.Logging.LogLevel.INFO, OnInfoMessage);
            _SFSInstance.SmartFox.RemoveLogListener(Sfs2X.Logging.LogLevel.WARN, OnWarnMessage);
            _SFSInstance.SmartFox.RemoveLogListener(Sfs2X.Logging.LogLevel.ERROR, OnErrorMessage);
            _SFSInstance.Reset();

        }
        #endregion


        #region Smartfox Message events
        void OnConnection(BaseEvent evt) {
            _logger.LogDebug("OnConnection");
            if ((bool)evt.Params["success"]) {
                Console.WriteLine("Connection established successfully");
                _logger.LogInformation("Connection established successfully");
                _logger.LogInformation("SFS2X API version: " + _SFSInstance.SmartFox.Version);
                _logger.LogInformation("Connection mode is: " + _SFSInstance.SmartFox.ConnectionMode);

                string cryptedPassword = MerxUtils.MerxCrypto.ComputeSha256Hash("test");
                _SFSInstance.SmartFox.Send(new Sfs2X.Requests.LoginRequest("Server", cryptedPassword));
            } else {
                _logger.LogInformation("Connection failed; is the server running at all?");

                // Remove SFS2X listeners and re-enable interface
                Reset();
            }
        }

        void OnConnectionLost(BaseEvent evt) {
            _logger.LogInformation("OnConnectionLost");
            Reset();
            _SFSInstance.SmartFoxNewInstance();
            SmartFoxInitListeners();
            Connect();
        }

        void OnLogin(BaseEvent evt) {
            _logger.LogDebug("OnLogin");
            User user = (User)evt.Params["user"];

            //_logger.LogInformation("log in successfully");
            _logger.LogInformation("Logged in as " + user.Name);
            Console.WriteLine("Logged in as " + user.Name);

            _SFSInstance.SmartFox.Send(new Sfs2X.Requests.JoinRoomRequest(_configuration.Room));
        }

        void OnLoginError(BaseEvent evt) {
            _logger.LogError("OnLoginError");
        }

        void OnRoomJoin(BaseEvent evt) {
            _ready = true;
            _logger.LogInformation("OnRoomJoin : " + _configuration.Room);
            Console.WriteLine("Room joined : " + _configuration.Room);

        }

        void OnRoomJoinError(BaseEvent evt) {
            _logger.LogError("OnRoomJoinError");
        }

        void OnInfoMessage(BaseEvent evt) {
            _logger.LogInformation("OnInfoMessage");
        }

        void OnWarnMessage(BaseEvent evt) {
            _logger.LogWarning("OnWarnMessage");
        }

        void OnErrorMessage(BaseEvent evt) {
            _logger.LogError("OnErrorMessage");
        }

        void OnExtension(BaseEvent evt) {
            string cmd = (string)evt.Params["cmd"];
            SFSObject dataObject = (SFSObject)evt.Params["params"];

            if (cmd == "sendToServer") {
                Sfs2X.Util.ByteArray bytes = dataObject.GetByteArray("message");

                MerxMessage.Message m = MerxMessage.Message.Deserialize(bytes.Bytes);
                int senderID = dataObject.GetInt("playerID");

                if (null != m) {
                    m.FromPlayerID = senderID;
                    OnNewMessage(m);
                    _logger.LogInformation("Reveived " + m.ToString() + " From " + m.FromPlayerID);
                }
            }
        }
        #endregion

        #region Console Command Management
        public void CommandUserList() {
            var users = _SFSInstance.SmartFox.LastJoinedRoom.UserList;
            string msg = "User List :" + Environment.NewLine;
            foreach (var user in users)
                msg += user.Name + Environment.NewLine;

            Console.WriteLine(msg);
        }
        #endregion
    }
}
