﻿using Sfs2X;
using Sfs2X.Core;
using System.Collections.Generic;

namespace ServerApp {
    public class SFSInstance {

        private SmartFox _smartFox = null;

        public SmartFox SmartFox { get => _smartFox; }

        public SFSInstance() {
            SmartFoxNewInstance();
        }

        public void SmartFoxNewInstance() {
            _smartFox = new SmartFox();
        }

        public void Reset() {
            _smartFox = null;
        }


    }
}
