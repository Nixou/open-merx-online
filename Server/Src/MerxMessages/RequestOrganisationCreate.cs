using System;

using MerxStructures;

namespace MerxMessage {

    /// <summary>
    /// Permet pour un joueur de demander la creation d'une organisation dans son compte
    /// </summary>
    [Serializable]
    public class RequestOrganisationCreate : Message {

        public string Name = "";

        public int CityID = 0;
    }
}
        