using System;

namespace MerxMessage {

    /// <summary>
    /// demande la suppression d'un mail d'une mailbox
    /// </summary>
    [Serializable]
    public class RequestMailBoxDeleteMessage : Message {

        public MerxStructures.MailInfos Mail = null;
    }
}
        