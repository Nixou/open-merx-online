using System;

namespace MerxMessage {

    /// <summary>
    /// Message permetant de dire que le joueur desire repondre a un offresur le market
    /// Cela peut etre pour acheter ou vendre des resources
    /// on trouve l'offre en question et une quantite comme le joueur peut acheter qu'une
    /// partie de l'offre de vente par exemple
    /// </summary>
    [Serializable]
    public class RequestFillOrders : Message {
        /// <summary> l'ordre qu'on achete </summary>
        public MerxStructures.MarketOrder order = null;

        /// <summary> la quantite reel a transiger sur cette offre </summary>
        public int qte;
    }
}
        