using System;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/// <summary>
/// classe de base pour tout les messages qui peuvent passent entre le client et le serveur
/// </summary>
namespace MerxMessage {

    [Serializable]
    public class Message {

        public enum TargetType {
            Target_Player,
            Target_Corp,
            Target_All
        }

        public int TargetID { get; private set; }
        public TargetType Target { get; private set; } 

        /// <summary> indique si c'est une requete ou une reponse de requete </summary>
        public bool IsAnswer { get; set; }

        /// <summary> Indique l'id du joueur qui a envoye cette requete, la valeur est ecras� par le serveur qui sait par quel canal arrive le message 
        /// un message ayant pour souce un server aura -1 comme valeur ici</summary>
        public int FromPlayerID { get; set; }

        public void SetTargetToPlayerID(int playerID) {
            Target = TargetType.Target_Player;
            TargetID = playerID;
        }

        /// <summary> serialiser ce message pour le transformer en binaire et l'envoyer </summary>
        /// <returns></returns>
        public byte[] Serialize() {

            MemoryStream stream = new MemoryStream();
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(stream, this);

            return stream.ToArray();
        }

        /// <summary> recreer un message a partir de donn�es binaire serialier </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static Message Deserialize(byte[] bytes) {

            MemoryStream stream2 = new MemoryStream(bytes);

            BinaryFormatter deserializer = new BinaryFormatter();
            Object o = deserializer.Deserialize(stream2);

            Message result = o as Message;
            return result;
        }

        public override string ToString() {
            return GetType().ToString();
        }
    }
}