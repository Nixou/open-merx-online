using System;
using System.Collections.Generic;

using MerxStructures;

namespace MerxMessage {

    /// <summary>
    /// requete permetant de recuperer la liste des offres d'achat et de vente sur le marche
    /// a un moment donne et pour un type de resource donne.
    /// </summary>
    [Serializable]
    public class RequestMarketDatas : Message {

        /// <summary> l'id de la resource </summary>
        public int resourceID = 0;

        /// <summary> la liste des offres de vente et d'achat de cette ressource </summary>
        public List<MarketOrder> Orders = new List<MarketOrder>();


    }
}
        