using System;
using System.Collections.Generic;

namespace MerxMessage {

    /// <summary>
    /// demande la mise a jour de la boite mail (retourne le contenue total de la boite mail)
    /// </summary>
    [Serializable]
    public class RequestMailBoxUpdate : Message {

        public MerxStructures.MailBoxInfos infos = null;

        public RequestMailBoxUpdate() {
            infos = new MerxStructures.MailBoxInfos();
        }
    }
}
        