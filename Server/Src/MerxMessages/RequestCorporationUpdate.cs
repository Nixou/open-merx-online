using System;

using MerxStructures;

namespace MerxMessage {

    /// <summary>
    /// Permet d'envoyer la mise a jour des infos d'une corporation aux clients
    /// todo : valider qu'aucun client ne peut l'envoyer
    /// </summary>
    [Serializable]
    public class RequestCorporationUpdate : Message {
        public int corpID = 0;
        public CorporationInfos corpInfo;
    }
}
        