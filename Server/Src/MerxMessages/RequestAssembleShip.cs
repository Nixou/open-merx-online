using System;
using System.Collections.Generic;

namespace MerxMessage {

    /// <summary>
    /// Requete permetant de demander l'assemblage d'un kit
    /// </summary>
    [Serializable]
    public class RequestAssembleShip : Message {

        /// <summary> la city ou assembler un kit </summary>
        public int cityID = 0;

        /// <summary> le type de ressource a assembler (un kit normalement) </summary>
        public int shipType = 0;
    }
}
        