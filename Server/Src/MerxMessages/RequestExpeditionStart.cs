using System;
using System.Collections.Generic;

namespace MerxMessage {

    /// <summary>
    /// Demander le lancement d'une expedition a partir d'une citee.
    /// </summary>
    [Serializable]
    public class RequestExpeditionStart : Message {
        public int fromCityID = 0;
        public int fromHangarID = 0;
        public List<int> vehicles = new List<int>();
    }
}
        