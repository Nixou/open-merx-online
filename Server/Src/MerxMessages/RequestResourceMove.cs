using System;

namespace MerxMessage {

    /// <summary>
    /// Demande envoye afin de faire transferer des resources d'un hangar vers un autre
    /// </summary>
    [Serializable]
    public class RequestResourceMove : Message {
        /// <summary> l'id de la cite depuis laquelle on deplace les resources </summary>
        public int cityFrom;

        /// <summary> l'id de la cite ou vont les resources </summary>
        public int cityTo;

        /// <summary> le type de resource deplacer </summary>
        public int resType = 0;

        /// <summary> la quantite a deplacer </summary>
        public int qte = 0;

        /// <summary> indique si le resultat est un succes(true) et donc que le deplacement est effectue </summary>
        public bool success = false;
    }
}
        