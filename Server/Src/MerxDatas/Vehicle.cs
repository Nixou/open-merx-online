using System;

namespace MerxData {

    /// <summary>
    /// Represente un vaisseau assembl�, utilisable, dans l'univers. Il peut etre soit dans un hangar soit en cours de mission.
    /// </summary>
    [Serializable]
    public class Vehicle : GameEntity{

        /// <summary> creation d'un vehicule </summary>
        /// <param name="universe">l'univers dans lequel va exister ce vaisseau</param>
        public Vehicle(Universe universe) : base(universe) {
        }

        public override void Update(long currentFrame) {
        }
    }
}