using System;
using System.Collections.Generic;

using MerxMessage;
using MerxStructures;

namespace MerxData {

    /// <summary>
    /// C'est le marche de l'univers, ici on a toute les propositions d'achat ou de vente qui existent dans l'univers.
    /// </summary>
    [Serializable]
    public class Market {

        int _marketUID = 0;

        Universe _universe = null;

        /// <summary> les offres de vente et d'achat en attente d'etre remplies</summary>
        Dictionary<int,MarketOrder> _orders = new Dictionary<int, MarketOrder>();
        Dictionary<int, int> _lastAverage = new Dictionary<int, int>();

        public Market(Universe universe) {
            _universe = universe;
        }

        /// <summary>
        /// ajouter un offre d'achat par un joueur
        /// </summary>
        /// <param name="h">la hangar ou placer les resources achet�es</param>
        /// <param name="resType"> le type de resources</param>
        /// <param name="qte">la quantite a acheter</param>
        /// <param name="price">le prix unitaire</param>
        public void AddSellOrder(Hangar h, int resType, int qte, int price) {
            //check resources
            if (h.GetResourceCount(resType) < qte)
                return;

            //Chercher s'il existe deja un order qu'on pourrait modifier
            MarketOrder order = null;
            foreach(var p in _orders) {
                if(p.Value.resID == resType && p.Value.cityID == h.City.ID && p.Value.price == price && p.Value.corpID == h.CorpID) {
                    p.Value.qte += qte;
                    order = p.Value;
                }
            }
                        
            if (order == null) {
                // si aucun order n'a ete modifie, on en cree un 
                MarketOrder sellOrder = new MarketOrder(_marketUID++, false, resType, h.City.ID, h.CorpID, qte, price,_universe.Frame);
                sellOrder.price = price;
                sellOrder.qte = qte;
                sellOrder.resID = resType;
                sellOrder.cityID = h.City.ID;
                _orders.Add(sellOrder.id, sellOrder);
            }
            
            // enlever les resources du hangar, elle devront etre remise en cas d'annulation de l'ordre
            h.LockResources(resType, qte);
        }

        public void AddBuyOrder(Hangar h, int resType, int qte, int price) {
            if (h == null || qte < 0 || price < 0)
                return;

            int total = price * qte;
            // tester si la corp a les moyen d'acheter
            Corporation corp = _universe.GetCorporation(h.CorpID);
            if (null == corp || (corp.Icu < total && corp.CorpID != 0))
                return;

            corp.Icu -= total;
            MarketOrder buyOrder = new MarketOrder(_marketUID++, true, resType, h.City.ID, h.CorpID, qte, price, _universe.Frame);

            _orders.Add(buyOrder.id, buyOrder);
        }

        public List<MarketOrder> GetSellOrder(int resType) {

            List<MarketOrder> result = new List<MarketOrder>();

            foreach(var s in _orders.Values) {
                if(s.resID == resType && !s.buying) {
                    result.Add(s);
                }
            }

            return result; 
        }

        public void UpdateOrder(MarketOrder o) {
            if (_orders.ContainsKey(o.id)){
                MarketOrder m = _orders[o.id];
                m.qte = o.qte;
                m.price = o.price;
            }
        }

        public List<MarketOrder> GetBuyOrder(int resType) {
            List<MarketOrder> result = new List<MarketOrder>();

            foreach(var s in _orders.Values) {
                if(s.resID == resType && s.buying) {
                    result.Add(s);
                }
            }
            return result;
        }

        /// <summary> recuperer tout les offres achat et vente pour un type de bien donn� </summary>
        /// <param name="resType">l'id du type d'objet</param>
        /// <returns>la liste de tout les offres d'achat et vente pour ce type d'objet</returns>
        public List<MarketOrder> GetOrders(int resType) {
            List<MarketOrder> result = new List<MarketOrder>();
            foreach(var s in _orders.Values) {
                if(s.resID == resType) {
                    result.Add(s);
                }
            }
            return result;
        }

        public void RemoveOrder(MarketOrder order) {
            if (_orders.ContainsKey(order.id)){
                _orders.Remove(order.id);
            }
        }

        public int GetAveragePrice(int resType) {
            int sum = 100;
            int count = 1;
            foreach(MarketOrder o in _orders.Values) {
                if(o.resID == resType) {
                    sum += o.price;
                    count++;
                }
            }

            return sum / count;
        }
   }
}