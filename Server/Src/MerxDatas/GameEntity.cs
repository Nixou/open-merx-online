using System;

namespace MerxData {

    /// <summary>
    /// Classe de base de tout les entit�e dans le jeu qui peuvent reagir.
    /// </summary>
    [Serializable]
    public class GameEntity {        

        /// <summary> l'id de l'entity, chaque ID doit etre unique dans l'univers </summary>
        public int ID { get; private set; }

        /// <summary> l'univers dans lequel existe cet entit� </summary>
        public Universe Universe { get; private set; }

        /// <summary> constructeur  </summary>
        /// <param name="universe"> l'univers dans laquel instancier l'objet</param>
        public GameEntity(Universe universe) {
            ID = universe.NextUID;
            Universe = universe;
            universe.AddEntity(this);
        }

        /// <summary> Indique a l'obet qu'il doit peut etre effectuer une action </summary>
        /// <param name="currentFrame">le frame actuel du monde</param>
        public virtual void Update(long currentFrame) {
        }

        public override int GetHashCode() {
            return ID;
        }

        public override bool Equals(object obj) {
            if (null == obj)
                return false;
            GameEntity entity = obj as GameEntity;
            if (null == entity)
                return false;

            if (entity.ID != ID)
                return false;
            
            return true;
        }
    }
}