using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using MerxStructures;

namespace MerxData {

    [Serializable]
    public class Universe {
        public long Frame { get; private set; } = 1;

        public Market Market { get; private set; }

        [NonSerialized()]
        /// <summary> un seul random pour tout l'univers pour s'assurer du determinisme </summary>
        private MerxUtils.MerxRandom _random;
        private int seedRandom = 12345;

        public MerxUtils.MerxRandom Random {
            get {
                if (_random == null)
                    _random = new MerxUtils.MerxRandom(seedRandom);
                return _random;
            }
            private set { _random = value; }
        }


        /// <summary> un container de toutes les villes de l'universs  </summary>
        private Dictionary<int, City> _cities = null;

        /// <summary> les corporation, les IDs sont les meme que celle du player </summary>
        private Dictionary<int, Corporation> _corporation = null;

        /// <summary> la liste de toutes les ressources existant </summary>
        public Dictionary<int, Resource> _resources = null;
        public Dictionary<int, Resource> Resources {
            get {
                if (null == _resources) {
                    LoadResources();
                }
                return _resources;
            }
        }

        /// <summary> toutes les entit�es du jeu, chaque item du jeu est une entity </summary>
        private Dictionary<int, GameEntity> _entities = null;
        private int _entityUIDs = 1;
        public int NextUID { get { return _entityUIDs++; } }

        /// <summary> les entites stockent ici les messages qu'ils veulent envoyer </summary>
        public Queue<MerxMessage.Message> messageToSend = new Queue<MerxMessage.Message>();

        [Serializable]
        private struct PlannedAction {
            public long TargetFrame;
            public GameEntity Entity;

            public override string ToString() {
                return "Plan : id = " + Entity.ID + " , frame = " + TargetFrame;
            }
        }

        /// <summary> c'est une liste des frames auxquels il faut reveiller un objet pour qu'il puisse se mettre a jour </summary>
        private LinkedList<PlannedAction> _planning = null;

        /// <summary>
        /// constructeur qui genere un univers random a partir d'un seed
        /// </summary>
        /// <param name="seed">la creation d'un univers avec le meme seed garanti qu'il soit identique</param>
        public Universe(int seed) {

            Market = new Market(this);

            //Random = new Random(seedRandom);

            if (seed == 0) {
                CreateTestUniverse();
            } else {
                CreateRandomUniverse(seed);
            }
        }

        private void CreateRandomUniverse(int seed) {
            CreateTestUniverse();
        }

        private void LoadResources() {
            //charger la liste des ressources ici
            _resources = new Dictionary<int, Resource>();
            _resources.Add(1, new Resource() { Id = 1, BasePrice = 1000, Volume = 10.0f });
            _resources.Add(2, new Resource() { Id = 2, BasePrice = 200, Volume = 10.0f });
            _resources.Add(3, new Resource() { Id = 3, BasePrice = 50000, Volume = 10.0f });
            _resources.Add(4, new Resource() { Id = 4, BasePrice = 10, Volume = 10.0f });
            _resources.Add(5, new Resource() { Id = 5, BasePrice = 10, Volume = 10.0f });
            _resources.Add(6, new Resource() { Id = 6, BasePrice = 10, Volume = 10.0f });
            _resources.Add(7, new Resource() { Id = 7, BasePrice = 20, Volume = 10.0f });
            _resources.Add(8, new Resource() { Id = 8, BasePrice = 20, Volume = 10.0f });
            _resources.Add(9, new Resource() { Id = 9, BasePrice = 50, Volume = 10.0f });
            _resources.Add(10, new Resource() { Id = 10, BasePrice = 50, Volume = 10.0f });
            _resources.Add(11, new Resource() { Id = 11, BasePrice = 100, Volume = 10.0f });
            _resources.Add(12, new Resource() { Id = 12, BasePrice = 200, Volume = 10.0f });
            _resources.Add(13, new Resource() { Id = 13, BasePrice = 500, Volume = 10.0f });
        }

        private void CreateTestUniverse() {
            _entities = new Dictionary<int, GameEntity>();
            _planning = new LinkedList<PlannedAction>();
            _cities = new Dictionary<int, City>();
            _corporation = new Dictionary<int, Corporation>();

            List<Recipe> recipies = new List<Recipe>();
            Recipe r = new Recipe();
            r.Time = 100;
            r.inputs.Add(4, 100);
            r.outputs.Add(2, 1);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(5, 100);
            r.outputs.Add(2, 2);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(6, 100);
            r.outputs.Add(2, 3);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(7, 100);
            r.outputs.Add(2, 4);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(8, 100);
            r.outputs.Add(2, 6);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(9, 100);
            r.outputs.Add(2, 8);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(10, 100);
            r.outputs.Add(2, 11);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(11, 100);
            r.outputs.Add(2, 15);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(12, 100);
            r.outputs.Add(2, 20);
            recipies.Add(r);
            r = new Recipe();
            r.Time = 100;
            r.inputs.Add(13, 100);
            r.outputs.Add(2, 33);
            recipies.Add(r);

            City albanel = new City(this);
            albanel.Name = "Albanel";
            _cities.Add(albanel.ID, albanel);

            City dolbeau = new City(this);
            dolbeau.Name = "Dolbeau";
            _cities.Add(dolbeau.ID, dolbeau);

            City normandin = new City(this);
            normandin.Name = "Normandin";
            _cities.Add(normandin.ID, normandin);

            City stMethode = new City(this);
            stMethode.Name = "StMethode";
            _cities.Add(stMethode.ID, stMethode);

            City stFelicien = new City(this);
            stFelicien.Name = "StFelicien";
            _cities.Add(stFelicien.ID, stFelicien);

            foreach (var itePair in _cities) {
                foreach (Recipe iteRecipe in recipies) {
                    itePair.Value.AddRecipe(iteRecipe);
                }
            }

            albanel.AddGate(dolbeau, 18.0);
            albanel.AddGate(normandin, 8.0);

            dolbeau.AddGate(albanel, 18.0);
            dolbeau.AddGate(stMethode, 24.0);

            normandin.AddGate(albanel, 8.0);
            normandin.AddGate(stMethode, 15.0);

            stMethode.AddGate(dolbeau, 24.0);
            stMethode.AddGate(normandin, 15.0);
            stMethode.AddGate(stFelicien, 12.0);

            stFelicien.AddGate(stMethode, 12.0);


            //une corpo NPC et une corpo joueur
            Corporation c = CreateCorporation(1, "Player1", 1);

            Corporation corpNPC = CreateCorporation(0, "NPC", 1);
            corpNPC.Icu = 1000000;

        }

        /// <summary>
        /// faire avancer l'univers d'un frame
        /// </summary>
        public void PlayOnFrame() {
            Frame++;
            if (_planning != null) {
                while (_planning.Count > 0 && _planning.First.Value.TargetFrame == Frame) {
                    PlannedAction a = _planning.First.Value;
                    a.Entity.Update(a.TargetFrame);
                    _planning.RemoveFirst();
                }
            }
        }

        public void PlayXFrames(int nbFrame) {
            for (int i = 0; i < nbFrame; i++) {
                PlayOnFrame();
            }
        }

        #region modifications
        public void AddEntity(GameEntity entity) {
            if (_entities.ContainsKey(entity.ID))
                return;

            _entities.Add(entity.ID, entity);
        }

        public void DeleteEntity(GameEntity entity) {
            //s'assurer d'aucun event sur cet entity
            if (_entities.ContainsKey(entity.ID)) {
                _entities.Remove(entity.ID);
            }
            if (_cities.ContainsKey(entity.ID)) {
                _cities.Remove(entity.ID);
            }
            if (_corporation.ContainsKey(entity.ID)) {
                _corporation.Remove(entity.ID);
            }
        }
        /// <summary>
        /// permet d'indiquer a l'univers qu'un entity veut etre reveill� a un frame donn�.
        /// </summary>
        /// <param name="targetFrame">le frame auquel le reveiller</param>
        /// <param name="entity">l'entit� a reveiller</param>
        public void RegisterEvent(long targetFrame, GameEntity entity) {

            PlannedAction p = new PlannedAction();
            p.TargetFrame = targetFrame;
            p.Entity = entity;

            bool inserted = false;
            LinkedListNode<PlannedAction> ite = _planning.First;
            while (ite != null) {
                if (ite.Value.TargetFrame == targetFrame && ite.Value.Entity.ID == entity.ID)
                    return;

                if (ite.Value.TargetFrame > p.TargetFrame) {
                    _planning.AddBefore(ite, p);
                    inserted = true;
                    break;
                }
                ite = ite.Next;
            }

            if (!inserted)
                _planning.AddLast(p);
        }

        #endregion

        #region informationGetters

        public Dictionary<int, City> GetCities() {
            return _cities;
        }

        public Corporation CreateCorporation(int playerID, string organisationName, int homeCity) {
            //tester si ce nom existe
            bool exist = false;
            foreach (var i in _corporation) {
                if (i.Value.Name == organisationName) {
                    Console.WriteLine("organisation exist : " + organisationName);
                    exist = true;
                    break;
                }
            }
            if (exist)
                return null;

            if (!_corporation.ContainsKey(playerID)) {
                if (!_cities.ContainsKey(homeCity)) {
                    return null;
                }

                City city = _cities[homeCity];

                Corporation c = new Corporation(playerID, organisationName, city, this);
                _corporation.Add(playerID, c);
                c.Icu = 100000;

                //on ajoute un hangar dans le HQ de l'organisation
                Hangar h = city.AddHangar(c.CorpID);

                //offrir une moto a l'organisation
                h.AddResource(3, 1);
                h.AssembleVehicle(3);
            }

            return _corporation[playerID];
        }

        public Corporation GetCorporation(int id) {
            if (_corporation.ContainsKey(id)) {
                return _corporation[id];
            }
            return null;
        }

        public List<Corporation> GetCorporations() {
            return new List<Corporation>(_corporation.Values);
        }

        public City GetCity(int cityID) {
            if (!_entities.ContainsKey(cityID))
                return null;

            City s = _entities[cityID] as City;
            return s;
        }

        public City GetCity(string name) {
            foreach (City s in _cities.Values) {
                if (name == s.Name)
                    return s;
            }
            return null;
        }

        private class Node {
            public City city;
            public double distance;
        }

        public List<City> GetPath(City fromCity, City toCity) {

            //implementation de djikstra
            Dictionary<int, Node> nodes = new Dictionary<int, Node>();
            foreach (City s in _cities.Values) {
                Node n = new Node();
                n.city = s;
                n.distance = float.MaxValue;
                nodes.Add(s.ID, n);
            }

            HashSet<int> visited = new HashSet<int>();
            visited.Add(fromCity.ID);
            nodes[fromCity.ID].distance = 0.0;
            Dictionary<int, int> visitedFrom = new Dictionary<int, int>();  //indique pour un noeud par quel chemin on y est arriv�

            //assigner les poids aux voisins du debut
            foreach (StarGate g in fromCity.Gates.Values) {
                nodes[g.TargetCity.ID].distance = g.Distance;
                visitedFrom.Add(g.TargetCity.ID, fromCity.ID);
            }

            while (true) {
                //trouver le noeud avec la distance la plus basse
                Node ite = null;
                foreach (Node n in nodes.Values) {
                    if (!visited.Contains(n.city.ID)) {
                        if (null == ite || ite.distance > n.distance) {
                            ite = n;
                        }
                    }
                }
                if (null == ite)
                    return null;    //ca devrait pas arriver s'il existe un chemin

                //le noeud est visit�
                visited.Add(ite.city.ID);

                if (ite.city.ID == toCity.ID) {
                    List<City> result = new List<City>();
                    result.Add(toCity);
                    int iteReturn = toCity.ID;
                    while (iteReturn != fromCity.ID) {
                        iteReturn = visitedFrom[iteReturn];
                        result.Add(nodes[iteReturn].city);
                    }
                    result.Reverse();
                    return result;
                }

                //ajuster les voisins
                foreach (StarGate s in ite.city.Gates.Values) {
                    if (!visited.Contains(s.TargetCity.ID)) {
                        Node n = nodes[s.TargetCity.ID];
                        if (n.distance > ite.distance + s.Distance) {
                            n.distance = ite.distance + s.Distance;
                            visitedFrom[n.city.ID] = ite.city.ID;
                        }
                    }
                }
            }
        }
        #endregion

        public override int GetHashCode() {
            return Convert.ToInt32(Frame) + _entities.Count + _entityUIDs;
        }

        public override bool Equals(object obj) {
            if (null == obj)
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            Universe other = obj as Universe;
            if (null == other)
                return false;

            if (null == _entities || null == other._entities)
                return false;

            foreach (int i in _entities.Keys) {
                if (!other._entities.ContainsKey(i))
                    return false;

                if (!other._entities[i].Equals(_entities[i]))
                    return false;
            }
            return true;
        }
    }
}