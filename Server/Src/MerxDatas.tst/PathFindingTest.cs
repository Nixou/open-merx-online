﻿using System.Collections.Generic;

using NUnit.Framework;

namespace MerxData.tst {

    [TestFixture]
    public class PathFindingTest {

        Universe _universe = null;

        [SetUp]
        public void Init() {
            _universe = new Universe(0);
        }

        [Test]
        public void SimplePathFind() {
            City s1 = _universe.GetCity("Albanel");
            City s2 = _universe.GetCity("Normandin");

            List<City> path = _universe.GetPath(s1, s2);

            Assert.AreEqual(2, path.Count);

        }

        [Test]
        public void MultiplePath() {
            City albanel = _universe.GetCity("Albanel");
            City stFelicien = _universe.GetCity("StFelicien");

            List<City> path = _universe.GetPath(albanel, stFelicien);

            Assert.AreEqual(4, path.Count);
            Assert.AreEqual("Albanel", path[0].Name);
            Assert.AreEqual("Normandin", path[1].Name);
            Assert.AreEqual("StFelicien", path[3].Name);
        }
    }
}
