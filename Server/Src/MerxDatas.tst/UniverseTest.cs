﻿using System.Collections.Generic;

using NUnit.Framework;

namespace MerxData.tst {

    [TestFixture]
    public class UniverseTest {


        [Test(Description ="test de sauvegarde")]
        public void Serialize() {
            Universe u = new Universe(0);

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter f = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            f.Serialize(stream, u);

            stream.Position = 0;
            Universe result = f.Deserialize(stream) as Universe;

            Assert.AreEqual(result, u);
        }

        [Test(Description = "test de sauvegarde complet d'un univers")]
        public void TestCompletUniverse() {

            Universe u1 = new Universe(0);

            int playerCorp = 1;

            //ajout de hangars
            City albanel = u1.GetCity("Albanel");

            Hangar hangarAlbanel = albanel.AddHangar(playerCorp);

            //ajouter une moto
            Vehicle moto = new Vehicle(u1);
            hangarAlbanel.AddVehicle(moto);

            Expedition exp1 = new Exploration(u1);
            List<Vehicle> vehicleExp1 = new List<Vehicle>();
            vehicleExp1.Add(moto);
            Assert.IsTrue(exp1.Start(albanel, hangarAlbanel, vehicleExp1));

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter f = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            f.Serialize(stream, u1);

            stream.Position = 0;
            Universe u2 = f.Deserialize(stream) as Universe;

            Assert.AreEqual(u2, u1);

            City albanel2 = u2.GetCity("Albanel");
            Assert.AreEqual(albanel, albanel2);

            Hangar hangarAlbanel2 = albanel2.GetHangar(playerCorp);
            Assert.IsNull(hangarAlbanel2.GetVehicle(moto.ID));

            u1.PlayXFrames(1001); ///terminer l'expedition
            u2.PlayXFrames(1001);

            Assert.NotNull(hangarAlbanel.GetVehicle(moto.ID));
            Assert.NotNull(hangarAlbanel2.GetVehicle(moto.ID));


        }
    }
}
