﻿using NUnit.Framework;

namespace MerxData.tst {

    [TestFixture]
    public class CorporationTest {


        [Test(Description ="serialization des donnes corporation")]
        public void Serialize() {
            Universe u = new Universe(0);

            Corporation c1 = u.GetCorporation(1);
            c1.Icu += 1000;

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter f = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            f.Serialize(stream, u);

            stream.Position = 0;
            Universe result = f.Deserialize(stream) as Universe;

            Assert.AreEqual(result, u);

            c1 = result.GetCorporation(1);
            Assert.AreEqual(101000, c1.Icu);
        }
    }
}
