﻿using System.Collections;

using UnityEngine;

public class GameSceneGUIManager : MonoBehaviour {

    private LUniverse _universe = null;
    private LocalDataManager _manager = null;

    [SerializeField]
    HeaderWidget _header = null;

    [SerializeField]
    GameObject _loading = null;

    [SerializeField]
    Transform _dragZone = null;

    public int LocalPlayerID { get; private set; }

    void Start() {
        _loading.gameObject.SetActive(true);

        _manager = LocalDataManager.Instance;

        LocalPlayerID = _manager.LocalPlayerID;

        if(null != _manager) {
            _universe = _manager.Universe;
            _universe.OnTimeChange += OnTimeChange;
        }

        StartCoroutine(Load());

    }

    private void OnDestroy() {
        if(null != _universe) {
            _universe.OnTimeChange -= OnTimeChange;
        }
    }

    private void OnTimeChange(long time) {
        if (null != _header) {
            _header.SetDate(time);
        }
    }

    public Transform DragZone { get { return _dragZone; } }

    IEnumerator Load() {
        while (true) {
            if (_manager.Ready()) {
                _loading.gameObject.SetActive(false);

                HeaderWidget hw = FindObjectOfType<HeaderWidget>();
                if(null != hw) {
                    hw.SetLocalCorporation(_manager.LocalCorporation);
                }
                break;
            }
            yield return new WaitForSeconds(0.5f);
        }
        yield break;
    }
}
