﻿using UnityEngine;
using UnityEngine.UI;

public class StationLineInfoWidget : MonoBehaviour {

    LCity _city = null;
    GameSceneGUIManager _GUIManager = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _stationName = null;

    [SerializeField]
    Button _hasHangar = null;

    [SerializeField]
    Button _openStationInfoButton = null;

    [SerializeField]
    Image _headquarter = null;

    private void Awake() {
        _GUIManager = FindObjectOfType<GameSceneGUIManager>();

        _hasHangar.onClick.AddListener(OnBuyHangar);
        if(null != _openStationInfoButton) {
            _openStationInfoButton.onClick.AddListener(OnShowStationinfosClic);
        }
    }

    private void OnDestroy() {
        SetCity(null);
    }

    public void SetCity(LCity city) {
        if(null != _city) {
            _city.OnChange -= _station_OnChange;
        }
        _city = city;
        if (null != _city) {
            _city.OnChange += _station_OnChange;
        }
        UpdateVisu();
    }

    private void _station_OnChange() {
        UpdateVisu();
    }

    private void UpdateVisu() {
        if(null == _city) {
            _stationName.text = "";
            _hasHangar.gameObject.SetActive(false);
        } else {
            _stationName.text = _city.Name;
            //verifier s'il la corporation du joueur a un hangar dans la station
            int corpPlayer = _GUIManager.LocalPlayerID;
            _hasHangar.interactable = !_city.Hangars.ContainsKey(corpPlayer);

            _headquarter.gameObject.SetActive(LocalDataManager.Instance.LocalCorporation.HomeCityID == _city.ID);
        }
    }

    private void OnShowStationinfosClic() {
        Window w = WindowSystem.Instance.NewWindow("StationInfos");

        StationInfoWidget widget = w.Content.GetComponentInChildren<StationInfoWidget>();
        widget.SetCity(_city);

        w.Show();
    }

    private void OnBuyHangar() {
        LocalDataManager localManager = LocalDataManager.Instance;

        MerxMessage.RequestBuyHangar request = new MerxMessage.RequestBuyHangar();
        request.CityID = _city.ID;

        localManager.SendRequest(request);
    }
}
