﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HangarWidget : WindowContentPrefab, IDropHandler {

    LHangar _hangar = null;

    [SerializeField]
    Transform _content = null;

    [SerializeField]
    Transform _vehicleContent = null;

    [SerializeField]
    HangarIconWidget _iconPrefab = null;

    [SerializeField]
    HangarVehicleIconWidget _vehicleIconPrefab = null;


    public void SetHangar(LHangar hangar) {
        if(null != _hangar) {
            _hangar.OnChange -= OnChange;
        }
        _hangar = hangar;
        if(null != _hangar) {
            _hangar.OnChange += OnChange;
            _window.Title = _hangar.City.Name;
            OnChange();
        }        
    }

    public LHangar GetHangar() {
        return _hangar;
    }

    private void OnDestroy() {
        SetHangar(null);
    }

    private void OnChange() {
        while(_content.childCount > 0) {
            Transform t = _content.GetChild(0);
            t.SetParent(null);
            Destroy(t.gameObject);
        }
        while (_vehicleContent.childCount > 0) {
            Transform t = _vehicleContent.GetChild(0);
            t.SetParent(null);
            Destroy(t.gameObject);
        }

        if (_hangar == null)
            return;

        Dictionary < int,int> qtes = _hangar.GetQtes();
        foreach(int i in qtes.Keys) {
            if(qtes[i] > 0) {
                HangarIconWidget o = Instantiate<HangarIconWidget>(_iconPrefab);
                o.transform.SetParent(_content);
                o.SetData(_hangar, i, qtes[i]);
            }
        }
        foreach(LVehicle id in _hangar.GetVehicles()) {
            HangarVehicleIconWidget o = Instantiate<HangarVehicleIconWidget>(_vehicleIconPrefab);
            o.transform.SetParent(_vehicleContent);
            o.SetData(_hangar, id);
        }
    }

    public void OnDrop(PointerEventData eventData) {

        HangarMovingIconWidget movingIcon = eventData.pointerDrag.GetComponent<HangarMovingIconWidget>();
        if (null == movingIcon)
            return;

        HangarIconWidget icon = movingIcon.Icon;
        if (null != icon) {
            //tester si le hangar cible != de la source
            LHangar fromHangar = icon.Hangar;
            if (null != fromHangar && fromHangar.City.ID != _hangar.City.ID) {

                WindowSystem ws = WindowSystem.Instance;
                Window w = ws.NewWindowModal("StackDivide");

                StackDivideWidget stackDivice = w.Content.GetComponentInChildren<StackDivideWidget>();
                stackDivice.SetDatas(icon);
                stackDivice.OnValidate += OnQteValidate;

                w.Show();
            }
        }
    }

    private void OnQteValidate(HangarIconWidget icon, int qte) {
        if (null != icon) {
            //tester si le hangar cible != de la source
            LHangar fromHangar = icon.Hangar;
            MerxMessage.RequestResourceMove moveReq = new MerxMessage.RequestResourceMove();
            moveReq.cityFrom = fromHangar.City.ID;
            moveReq.cityTo = _hangar.City.ID;
            moveReq.resType = icon.ResType;
            moveReq.qte = qte;

            LocalDataManager.Instance.SendRequest(moveReq);
        }
    }
}
