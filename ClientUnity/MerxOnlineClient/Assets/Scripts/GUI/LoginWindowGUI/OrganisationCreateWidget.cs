﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Widget permetant la creation d'une organisation quand le joueur n'en a pas
/// </summary>
public class OrganisationCreateWidget : MonoBehaviour {

    [SerializeField]
    Button CreateButton = null;

    [SerializeField]
    TMPro.TMP_InputField _organisationName = null;

    [SerializeField]
    TMPro.TMP_Dropdown _cityList = null;

    [SerializeField]
    MessageWindow _messageWindow = null;

    private System.Action _callback = null;
    private LCitiesList _cities = null;

    private void Awake() {
        if(null != CreateButton) {
            CreateButton.onClick.AddListener(OnCreateButtonClic);
        }
    }

    public delegate void testAction();

    public void Show(System.Action callback) {

        _cityList.ClearOptions();

        _cities = LocalDataManager.Instance.CitiesList;
        foreach (LCity c in _cities.GetCities()) {  
            _cityList.options.Add(new TMPro.TMP_Dropdown.OptionData(c.Name));
        }
        _cityList.value = 0;
        _cityList.RefreshShownValue();

        this.gameObject.SetActive(true);
        _callback = callback;
    }

    /// <summary>
    /// le joueur a cliqué sur le bouton creer une corpo, on envoie le message au serveur
    /// </summary>
    private void OnCreateButtonClic() {
        if (_organisationName.text == "")
            return;

        if (_cityList.value < 0)
            return;

        MerxMessage.RequestOrganisationCreate m = new MerxMessage.RequestOrganisationCreate();

        m.Name = _organisationName.text;

        LCity c = _cities.GetCities()[_cityList.value];
        m.CityID = c.ID;

        LocalDataManager.Instance.OnNewMessage += ManagerServerAnswer;
        LocalDataManager.Instance.SendRequest(m);
    }

    void ManagerServerAnswer(MerxMessage.Message m) {
        if(m is MerxMessage.RequestOrganisationCreate) {
            MerxMessage.RequestOrganisationCreate r = m as MerxMessage.RequestOrganisationCreate;
            if (null != r) {
                LocalDataManager.Instance.OnNewMessage -= ManagerServerAnswer;
                if (r.CityID == -1) {
                    // creation failed
                    _messageWindow.Show("La creation de l'organisation a échoué, change son nom");
                } else {                    
                    this.gameObject.SetActive(false);
                    // la reponse est arrivé, on relance l'init
                    _callback?.Invoke();
                }
            }
        }
    }

}
