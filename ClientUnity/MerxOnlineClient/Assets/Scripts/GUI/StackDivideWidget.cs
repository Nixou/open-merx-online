﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Interface permetant d'indiquer la quantite desire quand un utilisateur deplace un tas de resource sans vouloir deplacer la totalite
/// </summary>
public class StackDivideWidget : WindowContentPrefab {

    /// <summary> le bouton pour valider le deplacement une fois que la qte est donnee </summary>
    [SerializeField]
    Button _validateButton = null;

    /// <summary> le bouton pour annuler le deplacement </summary>
    [SerializeField]
    Button _cancelButton = null;

    /// <summary> un champs contennat la quantite a deplacer </summary>
    [SerializeField]
    TMPro.TMP_InputField _qte = null;

    /// <summary> le message qui indique quoi fournir comme donnee </summary>
    [SerializeField]
    TMPro.TextMeshProUGUI _message = null;

    public delegate void OnValidateAction(HangarIconWidget icon, int qte);
    public OnValidateAction OnValidate = null;

    int _min = 0;
    int _max = 0;
    int _currentQte = 0;
    HangarIconWidget _iconFrom = null;

    private void Awake() {
        if(null != _validateButton) {
            _validateButton.onClick.AddListener(OnOkButtonClic);
        }
        if(null != _cancelButton) {
            _cancelButton.onClick.AddListener(OnCancelClic);
        }
        if(null != _qte) {
            _qte.onValidateInput += OnQteChange;
        }
    }

    /// <summary>
    /// initializer la fenetre avec les données
    /// </summary>
    /// <param name="icon">l'icon qui est en train d'etre divise</param>
    public void SetDatas(HangarIconWidget icon) {
        _min = 0;
        _max = icon.Qte;
        _currentQte = icon.Qte;
        _iconFrom = icon;
        UpdateVisu();
    }

    private void UpdateVisu() {
        _qte.text = _currentQte.ToString();

        _message.text = "Entrer une quantite entre " + _min.ToString() + " et " + _max.ToString();
    }

    private void OnOkButtonClic() {
        OnValidate?.Invoke(_iconFrom, int.Parse(_qte.text));
        _window.CloseWindow();
    }

    private void OnCancelClic() {
        _window.CloseWindow();
    }

    private char OnQteChange(string text, int charIndex, char addedChar) {
        int finSelection = _qte.selectionFocusPosition;
        int debutSelection = _qte.selectionAnchorPosition;
        if(debutSelection > finSelection) {
            int swap = debutSelection;
            debutSelection = finSelection;
            finSelection = swap;
        }

        string tmp = text;
        if(finSelection != debutSelection) {
            tmp = tmp.Remove(debutSelection, finSelection - debutSelection);
            tmp = tmp.Insert(debutSelection, addedChar.ToString());
        } else {
            tmp = text.Insert(charIndex, addedChar.ToString());
        }

        int result = 0;
        if (int.TryParse(tmp, out result)) {
            if (result >= _min && result <= _max) {
                _currentQte = result;
                return addedChar;
            }
        }

        return '\0';
    }

}
