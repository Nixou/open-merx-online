﻿using UnityEngine;

public class MarketItemSelectorWidget : MonoBehaviour {

    public delegate void OnItemSelectionAction(string itemName, int prodId);
    public event OnItemSelectionAction OnItemSelect = delegate { };

    [SerializeField]
    MarketItemIconWidget iconPrefab = null;

    void Start() {

        while(transform.childCount > 0) {
            Transform t = transform.GetChild(0);
            MarketItemIconWidget i = t.GetComponent<MarketItemIconWidget>();
            if(null != i) {
                i.OnClic -= OnItemClic;
            }
            t.SetParent(null);
            Destroy(t.gameObject);
        }

        foreach(var p in ResourcesData.Instance.GetResources()) {
            MarketItemIconWidget item = Instantiate<MarketItemIconWidget>(iconPrefab);
            item.transform.SetParent(transform);
            item.OnClic += OnItemClic;
            item.SetItem(p.Value.name,p.Key);
        }
    }

    private void OnItemClic(string itemName, int prodId) {
        OnItemSelect(itemName,prodId);
    }
}
