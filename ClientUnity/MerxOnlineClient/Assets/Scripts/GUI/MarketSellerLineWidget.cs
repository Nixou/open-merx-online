﻿using UnityEngine;
using UnityEngine.UI;

using MerxStructures;

public class MarketSellerLineWidget : MonoBehaviour {

    [SerializeField]
    TMPro.TextMeshProUGUI qte = null;

    [SerializeField]
    TMPro.TextMeshProUGUI price = null;

    [SerializeField]
    TMPro.TextMeshProUGUI city = null;

    [SerializeField]
    Button buyButton = null;

    MarketOrder _order = null;

    private void Awake() {
        buyButton.onClick.AddListener(OnBuy);
    }

    public void SetOrder(MarketOrder order) {
        qte.text = order.qte.ToString();
        price.text = order.price.ToString() + "ICU";

        LCity s = LocalDataManager.Instance.Universe.GetCity(order.cityID);
        city.text = s.Name;

        buyButton.interactable = true;

        _order = order;
    }

    void OnBuy() {

        if (_order == null)
            return;

        MarketWidget marketWidget = GetComponentInParent<MarketWidget>();
        marketWidget.SelectOrder(_order);
    }
}
