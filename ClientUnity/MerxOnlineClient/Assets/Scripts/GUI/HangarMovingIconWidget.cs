﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HangarMovingIconWidget : MonoBehaviour, IDragHandler, IEndDragHandler{

    HangarIconWidget _icon = null;
    public HangarIconWidget Icon {
        get { return _icon; }
        set { _icon = value; GetComponent<Image>().sprite = ResourcesData.Instance.GetSprite(_icon.ResType); }
    }

    public void OnDrag(PointerEventData eventData) {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData) {
        Destroy(gameObject);
    }

}
