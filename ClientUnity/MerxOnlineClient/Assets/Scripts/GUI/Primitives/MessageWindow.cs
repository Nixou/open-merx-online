﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class MessageWindow : MonoBehaviour {

    [SerializeField]
    TMPro.TextMeshProUGUI _message = null;

    [SerializeField]
    Button _closeButton = null;

    CanvasGroup _canvasGroup = null;

    private void Awake() {
        _canvasGroup = GetComponent<CanvasGroup>();

        if(null != _closeButton) {
            _closeButton.onClick.AddListener(Hide);
        }
    }

    public void Show(string message) {
        if(null != _message) {
            _message.text = message;
        }
        _canvasGroup.alpha = 1.0f;
        _canvasGroup.blocksRaycasts = true;
        _canvasGroup.interactable = true;
    }

    private void Hide() {
        _canvasGroup.alpha = 0.0f;
        _canvasGroup.blocksRaycasts = false;
        _canvasGroup.interactable = false;
    }
}
