﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// un component qui permet a une vue de gerer des onglets
/// </summary>
public class TabsView : MonoBehaviour {

    [SerializeField]
    List<Button> tabs = new List<Button>();

    [SerializeField]
    List<Transform> tabsData = new List<Transform>();

    /// <summary>
    /// faire le lien entre les boutons[i] et le frame[i]
    /// </summary>
    private void Awake() {
        for(int i = 0; i < tabs.Count; i++) {
            int j = i;
            tabs[i].onClick.AddListener(delegate { buttonClic(j); });
        }
    }

    private void buttonClic(int i) {
        if(tabsData.Count > i && i >= 0) { 
            tabsData[i].transform.SetAsLastSibling();
        }
    }
}
