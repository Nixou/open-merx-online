﻿using UnityEngine;

public class MarketWidget : WindowContentPrefab {

    [SerializeField]
    MarketItemSelectorWidget _ItemsZone = null;

    [SerializeField]
    MarketDataWidget _DataZone = null;

    [SerializeField]
    MarketOrderDetailWidget _detailWidget = null;

    private void Start() {
        _DataZone.gameObject.SetActive(false);
        _ItemsZone.gameObject.SetActive(true);

        _ItemsZone.OnItemSelect += _ItemsZone_OnItemSelect;
        _DataZone.OnReturnClic += _DataZone_OnReturnClic;

        _detailWidget.gameObject.SetActive(false);

        _window.Title = "Market";
    }

    private void OnDestroy() {
        _ItemsZone.OnItemSelect -= _ItemsZone_OnItemSelect;
    }

    public void SelectOrder(MerxStructures.MarketOrder order) {
        _detailWidget.Show(order);
    }

    private void _ItemsZone_OnItemSelect(string itemName,int prodId) {
        _DataZone.SetProduct(itemName,prodId);
    }

    private void _DataZone_OnReturnClic() {
        _DataZone.gameObject.SetActive(false);
    }
}
