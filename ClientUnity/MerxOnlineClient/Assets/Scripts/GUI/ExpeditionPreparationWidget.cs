﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ExpeditionPreparationWidget : WindowContentPrefab {

    [SerializeField]
    TMPro.TextMeshProUGUI _status = null;

    [SerializeField]
    TMPro.TMP_Dropdown _cityFromList = null;

    [SerializeField]
    TMPro.TMP_Dropdown _expeditionType = null;

    [SerializeField]
    Transform _vehicleSelection = null;

    [SerializeField]
    Toggle _vehiculeSelectPrefab = null;

    [SerializeField]
    Transform _vehiculeList = null;

    [SerializeField]
    GameObject _vehicleSelectPrefab = null;

    [SerializeField]
    Button _launchButton = null;

    private List<LVehicle> _vehicles = new List<LVehicle>();
    private List<LCity> _cities = new List<LCity>();
    private LCity _cityFrom = null;

    private void Start() {
        _cityFromList.ClearOptions();
        _cityFromList.onValueChanged.AddListener(OnCitySelect);

        _launchButton.onClick.AddListener(OnLaunchButton);

        //ici on met tout les types d'expededition possible
        _expeditionType.ClearOptions();
        _expeditionType.options.Add(new TMPro.TMP_Dropdown.OptionData("Exploration"));
        _expeditionType.options.Add(new TMPro.TMP_Dropdown.OptionData("Exploitation"));
        _expeditionType.options.Add(new TMPro.TMP_Dropdown.OptionData("Transport"));
        _expeditionType.options.Add(new TMPro.TMP_Dropdown.OptionData("Extermination"));
        _expeditionType.value = 0;
        _expeditionType.RefreshShownValue();

        //trouver les cite ou on peut lancer une expedition
        int corpID = LocalDataManager.Instance.LocalPlayerID;

        _cityFromList.options.Add(new TMPro.TMP_Dropdown.OptionData("choisir cité"));

        _cities.Clear();
        foreach (LCity s in LocalDataManager.Instance.CitiesList.GetCities()) {
            LHangar h = s.GetHangar(corpID);
            if(null != h) {
                _cityFromList.options.Add(new TMPro.TMP_Dropdown.OptionData(s.Name));
                _cities.Add(s);
            }
        }
        _cityFromList.value = 0;
        _cityFromList.RefreshShownValue();

        UpdateValidateButton();
    }

    /// <summary> permet de remettre le formulaire a l'état initial </summary>
    public void ResetCity() {
        _vehicleSelection.gameObject.SetActive(false);
        _cityFromList.value = 0;
        _cityFromList.RefreshShownValue();
    }

    private void OnCitySelect(int value) {

        if (value < 0)
            return;

        _cityFrom = null;

        if (value == 0) {
            //ca du holder
        } else {
            _cityFrom = _cities[value - 1];

            //trouver tout les véhicules disponibles dans cette citee
            LHangar hangar = _cityFrom.GetHangar(LocalDataManager.Instance.LocalPlayerID);
            List<LVehicle> vehicles = hangar.GetVehicles();

            while(_vehiculeList.childCount > 0) {
                Transform t = _vehiculeList.GetChild(0);
                t.SetParent(null);
                Destroy(t.gameObject);
            }

            foreach (LVehicle v in vehicles) {
                GameObject o = Instantiate<GameObject>(_vehicleSelectPrefab);
                o.transform.SetParent(_vehiculeList);

                Toggle t = o.GetComponent<Toggle>();

                _vehicles.Add(v);
            }

            //afficher la vue de selection des vehicules
            _vehicleSelection.gameObject.SetActive(true);
        }
        UpdateValidateButton();
    }

    /// <summary>
    /// callback quand on change de cité, il faut remettre le forumulaire a neuf
    /// </summary>
    private void UpdateValidateButton() {
        bool result = false;

        string message = GetStateMessage();

        _status.text = message;

        if ( message == "Pret!"){
            result = true;
        }

        _launchButton.interactable = result;

    }

    /// <summary>
    /// Quand le bouton lancement est cliqué, on va lancer l'expeditin et fermer la
    /// fenetre
    /// </summary>
    private void OnLaunchButton() {

        MerxMessage.RequestExpeditionStart message = new MerxMessage.RequestExpeditionStart();

        message.fromCityID= _cityFrom.ID;
        message.fromHangarID = LocalDataManager.Instance.LocalPlayerID;
        foreach(LVehicle v in _vehicles) {
            message.vehicles.Add(v.ID);
        }

        LocalDataManager.Instance.SendRequest(message);

        Window w = GetComponentInParent<Window>();
        
        w.CloseWindow();
    }

    /// <summary> permet de recuperer le message a afficher a l'ecran sur l'etat actuel de preparation de l'expedition </summary>
    /// <returns></returns>
    private string GetStateMessage() {

        if(_cityFrom == null) {
            return "Aucune station de départ de choisie, il faut une station d'ou lancer l'expedition";
        }

        if(_vehicles.Count == 0) {
            return "Une expedition doit contenir au moins un vehicule, veuillez en selectionner un de disponible";
        }

        return "Pret!";
    }
}
