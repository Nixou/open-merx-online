﻿
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HangarIconWidget : MonoBehaviour , IBeginDragHandler, IDragHandler{

    [SerializeField]
    TMPro.TextMeshProUGUI _qte = null;

    
    [SerializeField]
    HangarMovingIconWidget _movingIconPrefab = null;

    public LHangar Hangar { get; private set; }

    public int Qte { get; private set; }
    public int ResType { get; private set; }

    private void Awake() {
        WindowContexteMenuLauncher context = GetComponent<WindowContexteMenuLauncher>();
        if(null != context) {
            context.GetMenuLinesCB = GetContextualLines;
            context.OnMenuLineClic += OnContextualClic;
        }
    }


    public void SetData(LHangar hangar, int type, int qte) {
        Qte = qte;
        ResType = type;
        _qte.text = qte.ToString();
        Hangar = hangar;

        Image i = GetComponent<Image>();
        i.sprite = ResourcesData.Instance.GetSprite(type);
    }

    
    void IBeginDragHandler.OnBeginDrag(PointerEventData eventData) {
        if (eventData.button != PointerEventData.InputButton.Left) {
            eventData.Reset();
            return;
        }

        GameSceneGUIManager manager = GetComponentInParent<GameSceneGUIManager>();

        HangarMovingIconWidget instance = Instantiate<HangarMovingIconWidget>(_movingIconPrefab);
        instance.transform.SetParent(manager.DragZone);
        instance.transform.position = Input.mousePosition;
        instance.Icon = this;

        eventData.pointerDrag = instance.gameObject;
        instance.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    void IDragHandler.OnDrag(PointerEventData eventData) {
        eventData.Reset();
    }

    private List<string> GetContextualLines() {
        List<string> result = new List<string>();
        
        result.Add("details");
        if(ResType == 3) {
            result.Add("assembler");
        }
        result.Add("detruire");

        return result;
    }

    private void OnContextualClic(string line) {
        switch (line) {
            case "assembler":

            MerxMessage.RequestAssembleShip r = new MerxMessage.RequestAssembleShip();
            r.cityID = Hangar.City.ID;
            r.shipType = ResType;

            LocalDataManager.Instance.SendRequest(r);

            break;
        }
    }
}
