﻿using System;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// Un Proxy permetant d'envoyer les requetes vers un serveur de simulation en local en utilisant
/// un memoryStream comme canal
/// </summary>
public class LocalRequestProxy : IRequestProxy{

    Queue<MemoryStream> _stream = null;

    public LocalRequestProxy(Queue<MemoryStream> stream) {
        _stream = stream;
    }

    public void CreateAccount(string username, string password) {
        throw new NotImplementedException();
    }

    public void SendRequest(MerxMessage.Message message) {
        lock (_stream) {
            MemoryStream stream = new MemoryStream(message.Serialize());
            _stream.Enqueue(stream);
        }
    }
}
