﻿
public class LVehicle {
    public int ID { get; private set; }
    public int VehicleType { get; private set; }

    public LVehicle(int id, int type) {
        ID = id;
        VehicleType = type;
    }
}
