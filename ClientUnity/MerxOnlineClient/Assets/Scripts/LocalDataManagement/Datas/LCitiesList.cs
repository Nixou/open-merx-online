﻿using System.Collections.Generic;

using MerxMessage;
using MerxStructures;

/// <summary>
/// Structure qui represente la liste des cites du monde
/// </summary>
public class LCitiesList {

    public delegate void EmptyAction();

    public event EmptyAction OnListChange = delegate { };

    LocalDataManager _manager = null;
    Dictionary<int, LCity> _cities = new Dictionary<int, LCity>();
    
    public LCitiesList(LocalDataManager manager) {
        _manager = manager;
        _manager.OnNewMessage += _manager_OnNewMessage;

        _manager.RequestCitiesList();

        OnListChange();
    }

    private void _manager_OnNewMessage(Message m) {
        RequestCities r = m as RequestCities;
        if (null != r) {
            foreach(CityInfo i in r.cities) {
                if (!_cities.ContainsKey(i.ID)) {
                    LCity station = LocalDataManager.Instance.Universe.GetCity(i.ID);
                    _cities.Add(i.ID, station);
                }
            }
        }
    }

    public List<LCity> GetCities() {
        return new List<LCity>(_cities.Values);
    }
}
