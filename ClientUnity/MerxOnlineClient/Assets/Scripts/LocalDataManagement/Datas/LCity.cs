﻿using System.Collections.Generic;

using MerxMessage;
using MerxStructures;

public class LCity {

    public delegate void OnChangeAction();
    public delegate void HangarAction(LHangar hangar);

    public event OnChangeAction OnChange = delegate { };
    public event HangarAction OnNewHangar = delegate { };

    public int ID { get; private set; }
    public string Name { get; private set; }

    /// <summary> la liste des corporation qui ont un hangar ici </summary>
    public Dictionary<int,LHangar> Hangars { get; private set; }

    private LocalDataManager _manager = null;

    public LCity(int id, LocalDataManager manager) {
        ID = id;
        Hangars = new Dictionary<int, LHangar>();
        _manager = manager;
    }

    public LCity(CityInfo info, LocalDataManager manager) {
        _manager = manager;
        Hangars = new Dictionary<int, LHangar>();
        Update(info);
    }

    public LHangar AddHangar(int corpID) {
        if (!Hangars.ContainsKey(corpID)) {
            LHangar h = new LHangar(corpID, this);
            Hangars.Add(corpID, h);

            OnNewHangar(h);
            OnChange();
        }

        return Hangars[corpID];
    }

    /// <summary>
    /// recuperer un hangar dans une station si il existe
    /// </summary>
    /// <param name="corpID">l'id de la corp a qui appartient le hangar</param>
    /// <returns></returns>
    public LHangar GetHangar(int corpID) {
        if (Hangars.ContainsKey(corpID)) {
            if (Hangars[corpID] == null) {
                Hangars[corpID] = new LHangar(corpID,this);
                OnNewHangar(Hangars[corpID]);
            }
            return Hangars[corpID];
        }
        return null;
    }

    public void Update(CityInfo info) {
        ID = info.ID;
        Name = info.Name;
        Dictionary<int,LHangar> newHangars = new Dictionary<int, LHangar>();
        foreach(int i in info.Hangars) {
            if (Hangars.ContainsKey(i)) {
                newHangars.Add(i, Hangars[i]);
            }
        }
        Hangars = newHangars;
    }
}
