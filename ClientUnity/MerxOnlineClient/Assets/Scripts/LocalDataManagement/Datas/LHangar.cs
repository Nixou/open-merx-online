﻿using System.Collections.Generic;

using MerxMessage;

public class LHangar {

    public delegate void OnChangeAction();
    public event OnChangeAction OnChange = delegate { };

    public LCity City { get; private set; }
    public int CorpID { get; private set; }

    private Dictionary<int, int> _qtes = new Dictionary<int, int>();
    private List<LVehicle> _vehicles = new List<LVehicle>();

    public LHangar(int corpID, LCity city) {
        City = city;
        CorpID = corpID;
    }

    public Dictionary<int,int> GetQtes() {
        return _qtes;
    }

    public List<LVehicle> GetVehicles() {
        return _vehicles;
    }

    public void Update(MerxStructures.HangarInfos infos) {
        _qtes = infos.qtes;

        _vehicles.Clear();
        foreach(int i in infos.vehicles) {
            LVehicle v = new LVehicle(i,1);
            _vehicles.Add(v);
        }

        OnChange();
    }
}
