﻿using System.Collections.Generic;

using MerxMessage;

public interface IEventProxy{

    Queue<Message> GetMessages();

}
