# Game design document #

Ce document a pour objectif de décrire le jeu Open Merx Online.

# Vue d'ensemble #

Nom : à déterminer
Genre : jeux vidéo de gestion, de commerce et de stratégie en ligne
Plateforme : client PC pour débuter

## Introduction ##

Open Merx Online est un projet de jeu de gestion en ligne se déroulant dans un univers persistant inspiré du Steampunk victorien. Chaque joueur prend les rênes d'une organisation sur une planète austère Halmo et cherche puissance et reconnaissance.

## Points originaux ##

-   Système économique gérée par les joueurs

-   Gameplay 4X : exploration, expansion, exploitation, extermination

-   Gestion de territoire

-   Collaboration et/ou concurrence entre joueur

-   Un seul univers persistent en constante évolution (Sandbox)

## Univers ##

Halmo est une planète aride en surface, avec une activité volcanique intense et des nappes phréatiques souterraines. Les Halmes, espèce intelligente, se sont regroupés en cités immense autour des sources chaudes disséminées sur la planète.
L’énergie géothermique est une des seules que peut procurer Halmo sans limite. La chaleur est stockée dans un cristal extrait de la planète, le Thale. Lorsqu’il est chauffé, ce dernier se transforme en gel et le reste même à température ambiante. Une fois mis en contact avec un catalyseur, le gel de Thale reprend sa forme cristalline en libérant l'énergie qu'il avait accumulée. Un litre de thale peut transformer 1000 litre d'eau en vapeur lors de sa cristallisation.
Ce cristal est la base de la pile à vapeur utilisé par l’ensemble de la machinerie halmienne, une cuve remplie d'eau dans laquelle une poche de gel est installée permettant à l'eau de devenir vapeur en ajoutant simplement un catalyseur. L’économie entière de Halmo est basées sur les machines à vapeurs fonctionnant au Thale, les organisations ont le contrôle sur les cités et permettent l’expansion des halmiens.

## Historique ##

Halmo en apparence inhabitable, est parsemé d'oasis de sources chaude ayant permis l’évolution d’une faune et d’une flore complexe. Les Halmes sont un produit direct de cette alchimie, ce peuple de nomades a peu à peu pris possession de la planète et de ses ressources. Avec l’apprentissage d’une agriculture difficile sur une terre sèche les peuples nomades deviennent alors sédentaires et se regroupent en cités autour des points d’eau.
L’éloignement des cités et les routes dangereuses limitent d’abord les contacts et échanges, chaque cité développe et protège ses propres coutumes et dialectes dérivés du langage commun.
Les débuts de l’industrialisation apparaissent de pair avec la découverte du thale, bouleversement total pour les halmiens. Les avancés en machinerie rapprochent les cités, les voyages se raccourcissent, la quêtes des minerais fait rage. Des prémices d’organisations se forment au cœur des cités. Ententes et mésententes forge déjà les alliances pour la domination des territoires.

## Contexte ##

Vous êtes un jeune entrepreneur qui vient d'obtenir un prêt de sa banque, vous comptez mener une entreprise florissante et vous démarquer de vos rivaux. A vous de choisir le chemin pour récolter gloire et richesses. Serez-vous un grand commerçant, un brigand réputé, le chef d'un conglomérat de production d'armement?

# Définitions #

Voici une liste des définitions utiles pour la compréhension du reste du document :

- Citée : Une cité est un ensemble de bâtiments situé autour d’une source géothermale. Elle offre un certain nombre de service et est propriété d’une organisation. Beaucoup de cités sont sous le contrôle des organisations non joueur.

- Expédition : Groupe qui s'éloigne d'une citée avec un objectif. Une fois l'objectif rempli, ils reviennent à la cité... si tout s'est bien passé. Les objectifs sont variés, de l’exploration à l’attaque en passant par le commerce

- Organisation : C’est une personne morale qui contrôle un ensemble d’avoirs : employés, véhicules ou citées. (Analogue à une entreprise dans la réalité), un joueur gère une organisation.

# Mécanismes #

Cette section décrit d'une façon générale les fonctionnalités du jeu. Les détails concernant les formules de calcul et chiffres seront précisés dans une autre section.

## La carte ##

La carte du monde est en théorie illimitée. Néanmoins la quantité de ressources transportable nécessaire aux voyages sont limités et rendront impossible les expéditions au-delà d’une certaine distance.

La carte est très dense au centre et décroit avec la distance, plus les citées sont espacées et plus la probabilité d'y trouver des ressources rares augmente.

L’environnement est principalement composé de désert, montagne aride et toundra. Les colonies sont situées autour de points géothermiques, source d’eau et d’énergie.

## Exploration ##

Le joueur peut envoyer des groupes effectuer des missions d’exploration. Le joueur indique la zone où envoyer le groupe et les véhicules qui en feront partie. L’expédition tentera de localiser des sites d’intérêts pendant un temps et reviendra à sa station de départ pour livrer les résultats. L'exploration est une forme d'expédition.

## Exploitation ##
	Non dispo en 0.1
L’exploitation est l’envoi d’un groupe pour prélever les ressources d’un site exploré précédemment. Le groupe devra être équipé en conséquence en fonction du type de ressources à exploiter. Le groupe sera affecté d’une mission et continue tant que la mission n’est pas soit remplie soit impossible à remplir. L'exploitation est également une forme d'expédition.


## Deplacement ##
	non dispo en 0.1
Le déplacement est une forme d'expédition permetant aux véhicules la constituant de partir d'une cité pour se rendre à une autre citée. Ces expéditions concernant des transport de longue distance ne sont pas accessible à tout les véhicules.

## Roaming ##
	non dispo en 0.1
Le roaming est une forme d'expédition dans laquelle les véhicules partent en chasses contre d'éventuel autre expéditions. Ciblant une zone, cette expédition attaquera les expéditions d'autre joueur qu'elle croise dans la dite zone. A noter que certaines zone sont surveillées et donc patrouillées par des forces non joueur qui cherchent à exterminer les véhicules en Roaming.


## Les Hangars ##

Un joueur qui veut posséder des biens doit pouvoir les stocker dans un hangar. le joueur pour louer des hangars dans les cités. Chaque hangar doit être payé d'une facon hebdomadaire : 

 - 10,000 ICU à la location
 - 1,000 ICU par semaine, prélevé automatiquement, si pas de fond le hangar est supprimé ainsi que son contenu.
 - 10 ICU par m3 y entrant (représente la main d'oeuvre pour le déplacement)
## Production ##
	non dispo en 0.1
Le joueur pourra transformer certaines ressources en vue de les vendre ou de les utiliser soit même. Cette transformation s’opère au sein des usines situées dans les citées. Pour lancer une production, le joueur doit posséder un entrepôt de stockage amont où se trouvent les ressources à transformer ainsi qu'une recette. Après avoir payé le tarif de l'usine et attendu le temps nécessaire, il recevra les produits transformés.

### Recettes ###

Les recettes sont des plans qui permettent aux usines de transformer certaines ressources en d’autres biens. Une recette nécessite une certaine quantité de ressources en entrée. Au terme de la production les nouvelles ressources sont produites. Cette opération prend un certain temps, indiqué dans la recette.

## Commerce ##

Afin de réaliser les actions qu'il désire, le joueur aura besoin soit d'argent soit de ressources. Le marché est utile pour cela. On peut y acheter des ressources dans le but de les transformer ou de les utiliser, ou vendre à profits sur le marché d'une autre cité. 

## Les véhicules ##

Les expéditions envoyées par le joueur seront constituées d’un ou plusieurs véhicules. Il existe différent type de véhicule et ce pour différent types de missions. Les véhicule sont produits par le joueur ou achetés sur le marché. Certains véhicules peuvent voler, la majorité est terrestre. Les véhicules sont stockés dans les entrepôts d’une organisation quand ils ne sont pas utilisés. La majorité des véhicules nécessitent seulement un pilote, plus rarement certains postes additionnels peuvent nécessiter du personnel additionnel. Enfin, certains véhicules sont autonomes au détriment de leur efficacité.

## Les trains ##

Les villes des organisations NPC sont reliées entre elles par des trains. Les trains permettent d'envoyer des ressources d'une facon sécurisée entre deux cité en payant le prix.

## Ressources humaines ##
	non dispo en 0.1
Les organisations ont besoin de main d'œuvre pour pouvoir réaliser les tâches qui leur permettent de prendre de la valeur. Ces employés sont embauchés et ont des capacités diverses qui évoluent avec l'expérience et la formation. Bien sûr qui dit employé dit salaire, il faudra donc que le gestionnaire maximise leur utilisation tout en essayant d'éviter au maximum de les perdre dans de tragiques accidents....

## Customisation des véhicules ##
	non dispo en 0.1
Lorsque des expéditions, constituées d'une flotte de véhicule, partent en mission, les véhicules pourront être modifiés afin de leur permettre d'effectuer certaines tâches ou simplement d'être plus efficace. On peut imaginer une augmentation du blindage en contrepartie d'une perte en espace de stockage pour un véhicule partant dans une zone a risque. 

## Combats ##

Enfin, un sujet qui touche toutes les expéditions, il existe un risque de rencontrer des groupes dont les intentions ne sont pas amicales. Il faut garder en tête que même si le joueur n'est pas en ligne, l'expédition suit son court. Quand deux expéditions enemies se rencontre, il y aura un combat un seul sortira vainqueur et l'autre perdra véhicules et employés, dur coup à l'organisation.

## Gestion du territoire ##
	non dispo en 0.1
Le joueur pourra installer des postes d'exploration, sorte de mini citée. Ceux-ci offriront un certain nombre de services mais devront être protégés. Cette fonctionnalité n'est pas prévue dans la 1ere version, mais c'est ce qui rendra le jeu intéressant à terme.

# Graphismes #

Il existe pour la 1ere version qu'une seule "vue" du jeu. La vue depuis l'entrepôt d'une citée. La vue est surtout esthétique, elle pourra afficher certaines informations de la cité. Cette vue propose à l'utilisateur des boutons ouvrant des interfaces permettant d'effectuer les démarches nécessaire de gestion.

## L’entête ##
Une barre en haut de l'écran permet de voir l'entrepôt qui est affiché, l'état du portefeuille de l'organisation et son nom. 

## La barre d'outil ##
C'est une barre constituée d'une série de bouton permetant de lancer chacun des interfaces utilisateurs.

# Fonctionnement des mécanismes #

## Exploration ##

Pour lancer une exploration le joueur doit ouvrir l'interface d'exploration. Cette interface lui permet de choisir la station depuis laquelle l'exploration est lancée.

En fonction de la ville, il pourra ensuite choisir la zone où l'expedition va aller chercher. Plus la zone est eloignée de la cité plus les découvertes potentielles sont intéressante.

Si l'expedition réussi, il lancera un random(0,1) + (Max(vehicule.ScanRange) / 100.0) : 
On regarde dans la zone en question et on retourne le POI avec la probabilité le plus basse qui est supérieur au random ci haut.

EX : POI : proba(0.10, 0.25 et 0.9), le random = 0.15, on retourne le POI avec probabilité de 0.25. Si on fait une autre expedition et qu'on a un random = 0.11 on retourne le POI O.9.

V0.1 : Enfin on charge les ressources possible dans les cargo des véhicules de l'expédition qui rentre à la citée.

## Les zones autour d'une cité ##

Pour le moment les zones autour d'une cité sont des cercles concentriques.
Ces zones sont numérotées de 0 pour la plus près et incrémente de 1 en allant vers l'extérieur. Chaque zone a une distance de 5km entre son centre et son extérieur. 

Ex : La zone 0 sont les 5 km autour de la cité. La zone 1 les 10km autour de la cité moins la zone 0 et ainsi de suite.

## Les Point of interest(POI) ##

Le Serveur crée dans les zones de chaque cité à interval régulier un certain nombre de point d'intérets. Pour déterminer s'il doit créer une point : 

Pour chaque zone, une fois par minute la cité regarde si elle fait apparaitre une ressource dans cette zone : 

MaxPointInZone = ((1 - (CiteSecurityLevel ^ 2)) * 0.75 + 0.25) * numeroZone * 10;

Proba =  (log(maxPointInZone + 1) - log(nbPointInZone+1)) / log(maxPointInZone + 1)

un random entre 0 et 1, si le resultat > proba alors on fait apparaitre un POI(point of interets)

quand un POI est instancié on lance encore une fois un rand[0,1] et on regarde dans la table de la dite zone ce qui peut etre instancié. Si plusieurs items dans la meme zone on 
choisi au hasard dans la categorie.

voir la liste des POI dans 
POI.csv

## La production et Les recettes ##

Le processus de production implique l'utilisation des recettes. Chaque recette doit contenir ces informations : 

* Duree = le temps en seconde que dure cette recette
* Inputs = une serie de paire, chaque pair indique un ID de resource et la quantite necessaire de cette ressource. Il faut que la totalité de ces ressources soit présente dans le hangar pour lancer la recette et ces ressources sont retirées lors du lancement de la recette.
* Ouputs = une série de paire, chaque pair indique un ID de resource et une quantité. Quand la recette se termine, ces quantités de ressources sont ajoutées au hangar du joueur.

La recette est achetée sur le marché pour les recettes de base, la R&D pourra a terme permettre de faire évoluer ces recettes.

Afin de transformer des ressources en autre chose, le joueur doit utiliser les usines ainsi que les recettes. Il faut donc qu'une usine soit disponible dans le cité ou se trouve le hangar. Le joueur y fourni la recette ainsi que les ressources nécessaire et lance la production. Une fois la durée de la recette terminé, les ressources produites sont ajoutées au hangar du joueur. 

Les recettes sont vendues pour le moment dans toutes les stations. Le prix varie comme les autres produits en fonction de la demande et du prix de base.

## Commerce des cités

Les citées vont compenser les parties des mécaniques non accessibles aux joueur. Au moment présent, les cités vont être en charge de transformer les roches et donc créer un marché pour ces ressources.

v0.1 : Les stations achete les objets dont elle a besoin pour la recette qu'elle produit et cela au prix de base au debut et chaque heure augmente le prix si toujours a sa recherche.

## Les brigands et le roaming ##

Les brigands sont un des seul goldSink du jeu a ce stade. La probabilité que son expédition soit attaqué par des brigands dépend de la sécurité de la zone traversée ainsi que de la composition de l'expédition.

ExpeditionModificator(0..1) = MaxValue(vehicule.signature)

zoneModificator(0..1) = 1 - (CitySecurityLevel ^ MaxValue(zoneDistanceTraverse))

Proba = 1 - (ExpeditionModificator + zoneModificator) / 2.0

Si une expédition de brigant est croisé il faut déterminer la composition de l'expédition : 


## Combats ##

Pour le moment les combats sont assez simples, chaque véhicule a des points de vie et effectue des dégats dans l'ordre d'initiative. Les véhicule avec initiative égales effectuent leurs dégats en mêmes temps. On boucle tant que la bataille n'est pas fini car l'un des côté n'à plus d'unités.  

Voici les probabilité de toucher : 

- Motos vs
 - Moto 50% 
 - pickup 75%
 - truck 90%
- Pickup vs
  - Moto 40%
  - Pickup 50%
  - truck 80%
- truck vs
  - Moto 20%
  - Pickup 40%
  - Truck 50% 

Un véhicule choisi sa cible de facon à tirer sur la cible qui lui est la plus dangereuse. Pour ce faire il va maximiser le calcul : probaDegatsSurMoi * degatsPotentiel.
si j'ai un vehicule qui me touche 50% du temps pour 2 degats (0.5 * 2 = 1) et un autre qui me touche 10% mais avec 100 degarts (0.1 * 100 = 10) je tire sur le second.
 
Dans une version future les attributs et configurations des véhicules apporterons plus d'intéret a effectuer une vrai simulation comme un RTS avec des IAs, cela permetra en plus d'ajouter des notions de terrains. Il faudra également permettre à une expédition de tenter de fuire.

## R&D ##
TODO

## Construction de cité ##
TODO

# Données #

Voici les lieux ou trouver / modifier les valeurs ou données de l'univers de jeu : 

## Les cités ##

- cite1
	- Sec : 1.0
- cite2
	- Sec : 1.0
- cite3
	- Sec : 1.0
- cite4
	- Sec : 1.0
- cite5
	- Sec : 1.0
- cite6
	- Sec : 0.9
- cite7
	- Sec : 0.9
- cite8
	- Sec : 0.9
- cite9
	- Sec : 0.9
- cite10
	- Sec : 0.8
- cite11
	- Sec : 0.8
- cite12 
	- sec : 0.8

## Ressources ##

Les infos concernant les ressources sont disponibles dans le fichier Resources.csv

## Recettes  ##

Les recettes sont également codées en dur dans l'univers pour le moment, il faudra les stocker dans une BDD pour gérer l'intégrité et l'exporter vers un fichier .dat pour client et serveur

## Les POI ##

Encore une fois, un ficher POI.csv est crée mais ce serait préférable de le faire directement en BDD avec export en .dat pour s'assurer de l'intégrité référentiel

## Les attributs ##

### les véhicules ####
 - Cargo : la taille en m3 de resource que peut contenir ce véhicule
 - Consuption : la quantite d'eau nécessaire pour faire 1km 
 - Damages : Le nombre de points de dégat que le véhicule effectue quand il touche. 
 - HP : Point de santé, quand il tombe à zéro ou moins le véhicule est détruit.
 - Init : l'initiative de véhicule, plus c'est 
 - haut plus le véhicule tirera rapidement dans la boucle
 - Signature : C'est la "taille" de l'objet quand il est scanné par un radar. Plus c'est gros plus on peut être repéré de loin
 - ScanRange : la portée de son scanner
 - Speed : Vitesse de base en km/h 
 - WaterTank : la quantité d'eau du reservoir  (pour 1l d'eau il faut 1 unit de thule)
 
## Les vehicules ##

Il faut garder en tête que les véhicules de cet univers sont tous motorisé à partir de vapeur et de steam cells. 
 - Autocross : C'est un vehicule intermediaire, assez rapide et qui peut transporter un petit chargement.
 
 - Pickup : C'est un vehicule plus lent et beaucoup moins agile, il resiste mieux au tir et permet de transporter une volume non négligeable.
 
 - Truck : Un gros camion, assez lent mais qui permet de transporter une bonne cargaison. Il peut extraire des cailloux d'un gisement.
 
 - Loader : C'est un camion qui permet d'extraire de bon volume de roche d'un gisement mais sans capacité de transport. Il est lent mais résistant.
 